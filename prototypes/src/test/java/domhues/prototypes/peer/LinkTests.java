package domhues.prototypes.peer;

import static org.junit.Assert.assertEquals;
import io.ipfs.multihash.Multihash;

import java.io.IOException;

import org.junit.Test;

import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.Link;
import domhues.prototypes.links.PrototypeReference;

public class LinkTests {
  

  PeerNode peer;

  /**
   * To start junit test, please change bounded IPFS peer node to your local settings
   * @throws IOException
   */
  public LinkTests() throws IOException{
    String ip = "127.0.0.1";
    int port = 5001;
    Multihash peer_id = Multihash.fromBase58("QmaXA8NViHrR8tqZsZbFa2T48fVmrgWkkawHHVXgyq9FVL");
    
    // Change IP, Port and PeerID for your local setup
    peer = new PeerNode(ip, port, peer_id);
  }
  
  @Test
  public void getEmptySegment() throws IOException{
    assertEquals("This value must be an empty string.",Link.getFirstSegment("//test"), "");  
  }
  
  @Test
  public void getLastEmptySegment() throws IOException{
    assertEquals("This value must be an empty string.", Link.getFirstSegment("/"), "");
  }
  
  @Test (expected = IOException.class)
  public void falseFormat() throws IOException{
    Link.getFirstSegment("test/");
  }
  
  @Test
  public void getSegment() throws IOException{
    assertEquals("This value must be 'test' ",Link.getFirstSegment("/test/"), "test");  
  }
  
  @Test
  public void getLastSegment() throws IOException{
    assertEquals("This value must be 'test' ",Link.getFirstSegment("/test"), "test");  
  }
  
  @Test
  public void deleteEmptySegment() throws IOException{
    assertEquals("This value must be an empty string", Link.deleteFirstSegment("//test"), "/test");
  }
  
  @Test
  public void deleteLastEmptySegment() throws IOException{
    assertEquals("This value must be an empty string", Link.deleteFirstSegment("/"), "");
  }
  
  @Test
  public void deleteSegment() throws IOException{
    assertEquals("This value must be an empty string", Link.deleteFirstSegment("/test1/test2"), "/test2");
  }
  
  @Test
  public void deleteLastSegment() throws IOException{
    assertEquals("This value must be an empty string", Link.deleteFirstSegment("/test"), "");
  }
  
  @Test (expected = IOException.class)
  public void deleteFalseFormat() throws IOException{
    Link.getFirstSegment("test/");
  }
  
  @Test
  public void changeMutableLinkTest() throws IOException{
    PrototypeExpression p = new PrototypeExpression.Builder(ID.of("ex:changelink")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference r = peer.putPrototypeExpression(p, true);
    peer.updateMutableLink(r.getMutableLink(),PrototypeExpression.ZERO, true);   
    assertEquals("Target is wrong", peer.ipfsApi.resolveLink(r.getMutableLink()), peer.ipfsApi.resolveLink(PrototypeExpression.ZERO));
  }
  
  @Test
  public void multihashToLink(){
    Multihash h = Multihash.fromBase58("QmSCL4BD3T138XioiVuMRNqRTTaSJ1ytRYhce5urDco84p");
    IpfsLink link = Link.fromHash(h);
    assertEquals("This value must be an empty string", link.toString(), "/ipfs/QmSCL4BD3T138XioiVuMRNqRTTaSJ1ytRYhce5urDco84p");
  }
  
  @Test (expected = IOException.class)
  public void LinkTooLong() throws IOException{
    Link.checkLinkLength(new IpfsLink("/ipfs/hash/link1"),2);
  }
  
  @Test
  public void LinkCorrectLength() throws IOException{
    Link.checkLinkLength(new IpfsLink("/ipfs/hash"),2);
  }
  
  @Test (expected = IOException.class)
  public void LinkTooShort() throws IOException{
    Link.checkLinkLength(new IpfsLink("/ipfs"),2);
  }
  
  

}
