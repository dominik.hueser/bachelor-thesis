package domhues.prototypes.peer;

import static org.junit.Assert.assertEquals;
import io.ipfs.multihash.Multihash;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import domhues.prototypes.examples.FileGenerator;
import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.PrototypeReference;

/**
 * Change the peer settings of the used to peers in init() to your local settings. 
 * @author dominikhuser
 *
 */
public class StandardMethodTests {

  static PeerNode peer1, peer2;
  
  static PrototypeExpression pe;
  static IpfsLink peLink;
  static Multihash kbHash;
  
  static String ip1;
  static int port1;
  static Multihash peer_id1;
  
  static String ip2;
  static int port2;
  static Multihash peer_id2;

  
  @BeforeClass
  public static void init() throws IOException {
    ip1 = "127.0.0.1";
    port1 = 5001;
    peer_id1 = Multihash.fromBase58("QmaXA8NViHrR8tqZsZbFa2T48fVmrgWkkawHHVXgyq9FVL");
    
    ip2 = "127.0.0.1";
    port2 = 5002;
    peer_id2 = Multihash.fromBase58("QmX4VrdvtWy11j7LQHB2p2J2RM7hrxJ4JfU1G558FYFjss");
 
    
    peer1 = new PeerNode(ip1, port1, peer_id1);
    peer2 = new PeerNode(ip2, port2, peer_id2);
    
    //publish a KB with which tests are done
    FileInputStream fs = new FileInputStream(FileGenerator.generateRandomKB(1, false, false, true));
    ArrayList<PrototypeReference> ref = peer1.putPrototypeExpression(fs, false);
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:StandardMethodTest"));
    for(PrototypeReference r : ref){
      kb.add(r.getImmutableLink());
    }
    kbHash = peer1.putKnowledgeBase(kb.build(), false);
    
    //publish a prototype expression in which tests were made
    pe  = new PrototypeExpression.Builder(ID.of("ex:newPrototypeExpression")).setBase(ref.get(0).getImmutableLink()).add(ID.of("ex:link1"), ref.get(1).getImmutableLink()).remove(ID.of("ex:link2"), ref.get(2).getImmutableLink()).build();
    peLink = peer1.putPrototypeExpression(pe, true).getImmutableLink();

  }
  
  @Test (expected = RuntimeException.class)
  public void daemonRunningTest() throws IOException {
    new PeerNode("127.0.0.1",5100, Multihash.fromBase58("QmX4VrdvtWy11j7LQHB2p2J2RM7hrxJ4JfU1G558FYFjss")); // not exisiting IPFS daemon
  }
  
  @Test
  public void getOwnKnowledgeBase() throws IOException{
    Multihash h  = peer1.getKnowledgeBaseHash(ID.of("kb:StandardMethodTest")); 
    assertEquals("The Multihash which was returned is wrong", h, kbHash);
  }
  
  @Test 
  public void getForeignKnowledgeBase() throws IOException{
    Multihash h  = peer2.getKnowledgeBaseHash(peer_id1, ID.of("kb:StandardMethodTest"));
    assertEquals("The Multihash which was returned is wrong", kbHash.toBase58(), h.toBase58());
  }
  
  @Test
  public void getPrototypeKnowledgeBase() throws IOException{
    Multihash h  = peer1.getKnowledgeBaseHash(peer_id1,ID.of("kb:StandardMethodTest"));
    assertEquals("The Multihash which was returned is wrong", kbHash.toBase58(), h.toBase58());
  }

  @Test
  public void getPrototypeExpression() throws IOException{
    PublishedPrototypeExpression ppe = peer1.getPrototypeExpression(peLink, false);
    assertEquals("wrong base", ppe.getBase().toString(), pe.getBase().toString());
    assertEquals("wrong link", ppe.getAddList().getPropertyAt(0).getName().toString(), "ex:link1");
    assertEquals("wrong link", ppe.getRemoveList().getPropertyAt(0).getName().toString(), "ex:link2");
  }
  
}
