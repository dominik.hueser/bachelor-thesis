package domhues.prototypes.peer;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;

import domhues.prototypes.expressions.IncomingChangeExpression;
import domhues.prototypes.expressions.IncomingPrototypeExpression;
import domhues.prototypes.links.ID;

public class GraphTests {

  @Test (expected = Error.class)
  public void cycleDetectionTest() {
    MutableGraph<String> graph = GraphBuilder.directed().build();
    graph.addNode("n1");
    graph.addNode("n2");
    graph.putEdge("n1", "n2");
    graph.putEdge("n2", "n1");
    ImmutableGraph<String> immGraph = ImmutableGraph.copyOf(graph);
    Parser.cycleDetection(immGraph);
  }
  
  @Test (expected = Exception.class)
  public void selfCycleDetectionTest() {
    MutableGraph<String> graph = GraphBuilder.directed().build();
    graph.addNode("n1");
    graph.putEdge("n1", "n1");
    ImmutableGraph<String> immGraph = ImmutableGraph.copyOf(graph);
    Parser.cycleDetection(immGraph);
  }
    
  @Test
  public void noCycleDetection(){
    MutableGraph<String> graph = GraphBuilder.directed().build();
    graph.addNode("n1");
    graph.addNode("n2");
    graph.addNode("n3");
    
    graph.putEdge("n1", "n2");
    graph.putEdge("n2", "n3");
    graph.putEdge("n1", "n3");
  }
  
  @Test
  public void topologicalOrderCheck() throws IOException{
    MutableGraph<String> mGraph = GraphBuilder.directed().build();
    mGraph.addNode("ex:n1");
    mGraph.addNode("ex:n2");
    mGraph.addNode("ex:n3");  
    mGraph.putEdge("ex:n1", "ex:n2");
    mGraph.putEdge("ex:n2", "ex:n3");
    mGraph.putEdge("ex:n1", "ex:n3");  
    ImmutableGraph<String> graph = ImmutableGraph.copyOf(mGraph);
    
    HashMap<String, IncomingPrototypeExpression> map = new HashMap<String, IncomingPrototypeExpression>();
    IncomingPrototypeExpression n3 = new IncomingPrototypeExpression.Builder(ID.of("ex:n3"), new IncomingChangeExpression(ID.of("ex:t"),ID.of("proto:P_0"), false)).build();
    IncomingPrototypeExpression n2 = new IncomingPrototypeExpression.Builder(ID.of("ex:n2"), new IncomingChangeExpression(ID.of("ex:t"),ID.of("ex:n3"), false)).build();
    IncomingPrototypeExpression n1 = new IncomingPrototypeExpression.Builder(ID.of("ex:n1"), new IncomingChangeExpression(ID.of("ex:t"),ID.of("ex:n2"), false)).add(ID.of("ex:l1"),ID.of("ex:n3"),false).build();
    map.put("ex:n3", n3);
    map.put("ex:n2", n2);
    map.put("ex:n1", n1);
    
    ArrayList<IncomingPrototypeExpression> list = new ArrayList<IncomingPrototypeExpression>();
    list.add(n3);
    list.add(n2);
    list.add(n1);
    ArrayList<IncomingPrototypeExpression> result =  Parser.topologicalOrder(graph, map);
    assertEquals("Topological Order must be proto:P_0, n3, n2, n1", list, result);
  }
  

}
