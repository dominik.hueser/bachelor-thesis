package domhues.prototypes.peer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Test;

public class FileImportTests {

  @Test (expected = Error.class)
  public void addNotInFile() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("addNotInFile.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test (expected = Error.class)
  public void allInAddSet() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("allInAddSet.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test (expected = Error.class)
  public void baseNotInFile() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("baseNotInFile.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test
  public void definedExpressionTwice() throws IOException {
    // same IRI is allowed due to hash is used for PE identification.
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("definedExpressionTwice.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test (expected = Error.class)
  public void protoZeroInFile() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("protoZeroInFile.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }

  @Test (expected = Error.class)
  public void removeNotInFile() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("removeNotInFile.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test
  public void positiveTest() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("positiveTest.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test (expected = IOException.class)
  public void wrongIRIFormat() throws IOException {

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("wrongIRIFormat.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
  
  @Test (expected = Error.class)
  public void wrongLinkFormat() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("wrongLinkFormat.proto").getFile());
    FileInputStream fStream = new FileInputStream(file);
    Parser.getTopologcialOrder(fStream);
  }
}
