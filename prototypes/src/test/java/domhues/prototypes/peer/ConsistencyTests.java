package domhues.prototypes.peer;

import io.ipfs.multihash.Multihash;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import domhues.prototypes.examples.FileGenerator;
import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.knowledgebase.FixedPrototypeKB;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.IpnsLink;
import domhues.prototypes.links.Link;
import domhues.prototypes.links.PrototypeReference;


/**
 * Change Peer Settings (IP Port and PeerID to local settings)
 * @author dominikhuser
 *
 */
public class ConsistencyTests {

  PeerNode peer;

  public ConsistencyTests() throws IOException{
    String ip = "127.0.0.1";
    int port = 5001;
    Multihash peer_id = Multihash.fromBase58("QmaXA8NViHrR8tqZsZbFa2T48fVmrgWkkawHHVXgyq9FVL");
    peer = new PeerNode(ip, port, peer_id);
  }
  
  @Test (expected = Error.class)
  public void protoZeroCheck() throws IOException {
      PrototypeExpression pZero = new PrototypeExpression.Builder(ID.of("proto:P_0")).build();
      IpfsLink pZeroLink = peer.putPrototypeExpression(pZero, true).getImmutableLink();
      PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:protoZeroCheck"));
      kb.add(pZeroLink);
      FixedPrototypeKB fKb = kb.build().fix(peer);
      fKb.consistencyCheck();
  }
  
  @Test (expected = Error.class)
  public void mutableCycleCheck() throws IOException{
   try{
    PrototypeExpression a1 = new PrototypeExpression.Builder(ID.of("ex:t1")).setBase(new IpnsLink("/ipns//prototypes/0")).build();
    PrototypeReference ref1 = peer.putPrototypeExpression(a1, false);
    
    PrototypeExpression a2 = new PrototypeExpression.Builder(ID.of("ex:t2")).setBase(ref1.getImmutableLink()).build();
    PrototypeReference ref2 = peer.putPrototypeExpression(a2, true);

    peer.updateMutableLink(new IpnsLink("/ipns//prototypes/0"), ref2.getImmutableLink(), true);
   
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:protoZeroCheck"));
    kb.add(ref1.getImmutableLink());
    kb.add(ref2.getImmutableLink());
    PrototypeKB pKb = kb.build();
    peer.putKnowledgeBase(pKb, true);
    pKb.fix(peer).consistencyCheck();
   }catch(Exception e){
     e.printStackTrace();
   }
  }
  
  @Test (expected = Error.class)
  public void completedPrototypeSet() throws IOException{
    FileInputStream fs = new FileInputStream(FileGenerator.generateBaselineKB(3, true, false));
    ArrayList<PrototypeReference> ref = peer.putPrototypeExpression(fs, true);
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:completedPrototypeSet"));
    for(PrototypeReference r : ref){
      kb.add(r.getImmutableLink());
    }
    PrototypeKB pKb = kb.build();
    ArrayList<Link> list = pKb.getPrototypeExpressions();
    kb.remove(list.get(0)); 
    pKb.fix(peer).consistencyCheck();
  }
  
  @Test 
  public void positiveOnlineConsistencyTest() throws IOException{
    FileInputStream fs = new FileInputStream(FileGenerator.generateRandomKB(1, true, false, true));
    ArrayList<PrototypeReference> ref = peer.putPrototypeExpression(fs, true);
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:positiveOnlineConsistency"));
    for(PrototypeReference r : ref){
      kb.add(r.getImmutableLink());
    }
    PrototypeKB pKb= kb.build();
    peer.putKnowledgeBase(pKb, true);
    Multihash h  = peer.getKnowledgeBaseHash(ID.of("kb:positiveOnlineConsistency"));
    FixedPrototypeKB newKb = peer.getKnowledgeBase(h); 
    newKb.consistencyCheck();
  }

  @Test 
  public void positiveOfflineConsistencyTest() throws IOException{
    FileInputStream fs = new FileInputStream(FileGenerator.generateRandomKB(3, true, false, true));
    ArrayList<PrototypeReference> ref = peer.putPrototypeExpression(fs, true);
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:positiveConsistency"));
    for(PrototypeReference r : ref){
      kb.add(r.getImmutableLink());
    }
    PrototypeKB pKb = kb.build();
    
    pKb.fix(peer).consistencyCheck();
  }
  

}
