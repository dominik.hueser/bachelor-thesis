package domhues.prototypes.examples;

import io.ipfs.multihash.Multihash;

import java.io.IOException;
import java.util.HashMap;

import domhues.prototypes.expressions.Prototype;
import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.knowledgebase.FixedPrototypeKB;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.PrototypeReference;
import domhues.prototypes.peer.PeerNode;

public class ExampleUsage {

  static PrototypeReference manRef;
  static PrototypeReference carlRef;
  
  static String ip_peer1;
  static int port_peer1;
  static Multihash id_peer1;
  
  static String ip_peer2;
  static int port_peer2;
  static Multihash id_peer2;

  
  /**
   * Create several objects, change them, publish, build KB and compute the fixpoint and do consisntecy check once at the same node and once from another one.
   * @param args
   *   0: IP of peer 1
   *   1: Port of peer 1
   *   2: Peer ID (as String) of peer 1
   *   3: IP of peer 2
   *   4: Port of peer 2
   *   5: Peer ID (as String) of peer 2

   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    
    if(args.length!=6){
      throw new Error("Wrong amount of arguments");
    }
    
    ip_peer1 = args[0];
    port_peer1 = Integer.valueOf(args[1]);
    id_peer1 = Multihash.fromBase58(args[2]);
    
    ip_peer2 = args[3];
    port_peer2 = Integer.valueOf(args[4]);
    id_peer2 = Multihash.fromBase58(args[5]);
    
    immutablePrototypeExample();
    createExpressionFromExistingOne();
    removeAllExpression();
    requestKBFromOtherNode();
  }

  /**
   * Create immutable PEs.
   * @throws IOException
   */
  public static void immutablePrototypeExample() throws IOException {
    PeerNode peer = new PeerNode(ip_peer1, port_peer1, id_peer1);

    PrototypeExpression female = new PrototypeExpression.Builder(ID.of("ex:female")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference femaleRef = peer.putPrototypeExpression(female, false);

    PrototypeExpression male = new PrototypeExpression.Builder(ID.of("ex:male")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference maleRef = peer.putPrototypeExpression(male, false);

    PrototypeExpression human = new PrototypeExpression.Builder(ID.of("ex:human")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference humanRef = peer.putPrototypeExpression(human, false);

    PrototypeExpression woman = new PrototypeExpression.Builder(ID.of("ex:woman")).setBase(PrototypeExpression.ZERO).add(ID.of("ex:sex"), femaleRef.getImmutableLink()).add(ID.of("ex:species"), humanRef.getImmutableLink()).build();
    PrototypeReference womanRef = peer.putPrototypeExpression(woman, false);

    PrototypeExpression man = new PrototypeExpression.Builder(ID.of("ex:man")).setBase(womanRef.getImmutableLink()).remove(ID.of("ex:sex"), femaleRef.getImmutableLink()).add(ID.of("ex:sex"), maleRef.getImmutableLink()).build();
    manRef = peer.putPrototypeExpression(man, false);
    peer.publishChanges();
  }

  /**
   * create PE from exisiting one
   * @throws IOException
   */
  public static void createExpressionFromExistingOne() throws IOException {
    PeerNode peer = new PeerNode(ip_peer1, port_peer1, id_peer1);

    PublishedPrototypeExpression man = peer.getPrototypeExpression(manRef.getImmutableLink(), false);

    PrototypeExpression carlsName = new PrototypeExpression.Builder(ID.of("ex:carlcarlson")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference carlsNameRef = peer.putPrototypeExpression(carlsName, false);

    PrototypeExpression carl = new PrototypeExpression.Builder(ID.of("ex:carl")).setBase(man).add(ID.of("ex:name"), carlsNameRef.getImmutableLink()).build();
    carlRef = peer.putPrototypeExpression(carl, false);

    peer.publishChanges();
  }

  
  /**
   * Remove all expression and create KB (incl. consistnecy and fixpoint)
   * @throws IOException
   */
  public static void removeAllExpression() throws IOException {
    PeerNode peer = new PeerNode(ip_peer1, port_peer1, id_peer1);

    PrototypeExpression address = new PrototypeExpression.Builder(ID.of("ex:SchlossAllee")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference addressRef = peer.putPrototypeExpression(address, false);

    PrototypeExpression inhabitant1 = new PrototypeExpression.Builder(ID.of("ex:elena")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference inhabitant1Ref = peer.putPrototypeExpression(inhabitant1, false);

    PrototypeExpression inhabitant2 = new PrototypeExpression.Builder(ID.of("ex:carlo")).setBase(PrototypeExpression.ZERO).build();
    PrototypeReference inhabitant2Ref = peer.putPrototypeExpression(inhabitant2, false);


    PrototypeExpression skyscraper = new PrototypeExpression.Builder(ID.of("ex:skyscraper")).setBase(PrototypeExpression.ZERO).add(ID.of("ex:address"), addressRef.getImmutableLink()).add(ID.of("ex:inhabitant"), inhabitant1Ref.getMutableLink()).add(ID.of("ex:inhabitant"), inhabitant2Ref.getMutableLink()).build();
    PrototypeReference skyscraperRef = peer.putPrototypeExpression(skyscraper, false);

    PrototypeExpression newParkArea = new PrototypeExpression.Builder(ID.of("ex:NewSchlossAllePark")).setBase(skyscraperRef.getImmutableLink()).remove(ID.of("ex:inhabitant"), PrototypeExpression.ALL).build();
    PrototypeReference parkRef = peer.putPrototypeExpression(newParkArea, false);

    //Build the KB
    PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:city"));
    kb.add(addressRef.getImmutableLink());
    kb.add(inhabitant2Ref.getImmutableLink());
    kb.add(skyscraperRef.getImmutableLink());
    kb.add(inhabitant1Ref.getImmutableLink());
    kb.add(parkRef.getImmutableLink());

    PrototypeKB pKb = kb.build();
    
    // publish KB updates dir because flag is true
    peer.putKnowledgeBase(pKb, true);

    //fix the mutable links of Items of the KB.
    FixedPrototypeKB fKb = pKb.fix(peer);
    fKb.consistencyCheck();
    HashMap<Multihash, Prototype> res = fKb.computeFixpoint();

    System.out.println("########Requested KB from the same node########");
    for (Prototype pro : res.values()) {
      pro.print();
    }

    peer.publishChanges();

    //peer.depublishKB("city", true);
  }
  
  static void requestKBFromOtherNode() throws IOException{
    PeerNode peer2 = new PeerNode(ip_peer2, port_peer2, id_peer2);

    Multihash reqCityHash = peer2.getKnowledgeBaseHash(id_peer1, ID.of("kb:city")); 
    
    FixedPrototypeKB reqCity = peer2.getKnowledgeBase(reqCityHash);
    
    reqCity.consistencyCheck();

    HashMap<Multihash, Prototype> res = reqCity.computeFixpoint();
    
    //Print Result from requested Node
    System.out.println("########Requested KB from a second node########");
    for (Prototype pro : res.values()) {
      pro.print();
    }
  }


}
