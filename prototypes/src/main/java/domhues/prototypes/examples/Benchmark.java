package domhues.prototypes.examples;


import io.ipfs.multihash.Multihash;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

import domhues.prototypes.knowledgebase.FixedPrototypeKB;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.PrototypeReference;
import domhues.prototypes.peer.PeerNode;

/**
 * This class contains a Benchmark test which could be configured by it's attributes.
 * A soloTest is run for baseline(10/12/13), propline(10/12/13), random(1,5,10), and blocks(1,5,10).
 * A soloTest measures time consumption of:
 * 1) create and put prototype expressions
 * 2) publish new expressions
 * 3) create a KB out of these PE (20% mutable links)
 * 4) publish new KB
 * 5) Request KB from another node (incl. download of all PE of the KB)
 * 6) Download the KB's PE, only.
 * 7) Consistency Check
 * 8) Fixpoint Computation
 * 
 * @author dominikhuser
 *
 */
public class Benchmark {
  
  private static String newLine = System.getProperty("line.separator");
  
  private static int repetitions = 40;
  private static boolean randomMutability = true;
  private static boolean shuffle= true;

  private static PeerNode peer;
  private static PeerNode peer2;

  /**
   * 
   * @param args
   *   0: IP of peer 1
   *   1: Port of peer 1
   *   2: Peer ID (as String) of peer 1
   *   3: IP of peer 2
   *   4: Port of peer 2
   *   5: Peer ID (as String) of peer 2
   * @throws IOException
   * @throws InterruptedException
   */
  public static void main(String[] args) throws IOException, InterruptedException{
    
      if(args.length!=6){
        throw new Error("Wrong amount of arguments");
      }
    
    //peer = new PeerNode("127.0.0.1", 5001, Multihash.fromBase58("QmaXA8NViHrR8tqZsZbFa2T48fVmrgWkkawHHVXgyq9FVL"));
    //peer2 = new PeerNode("127.0.0.1", 5002, Multihash.fromBase58("QmX4VrdvtWy11j7LQHB2p2J2RM7hrxJ4JfU1G558FYFjss"));
    peer = new PeerNode(args[0], Integer.valueOf(args[1]), Multihash.fromBase58(args[2]));
    peer2 = new PeerNode(args[3], Integer.valueOf(args[4]), Multihash.fromBase58(args[5]));
       
    peer = peer.reset(); // reset ipns, unpin everything, cg
    peer2 = peer2.reset();
       
    System.out.println("##############BASELINE(10)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("baseline",10);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    System.out.println("##############BASELINE(12)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("baseline",12);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
 
    System.out.println("##############BASELINE(13)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("baseline",13);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    System.out.println("##############PROPLINE(10)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("propline",10);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    System.out.println("##############PROPLINE(12)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("propline",12);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    System.out.println("##############PROPLINE(13)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("propline",13);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }

    
    System.out.println("##############RANDOM(1000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("random",1000);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }   
   
    System.out.println("##############RANDOM(5000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("random",5000);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    
    System.out.println("##############RANDOM(10000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("random",10000);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
 
    System.out.println("##############BLOCKS(1 times 1000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("blocks",1);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }
    
    System.out.println("##############BLOCKS(5 times 1000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("blocks",5);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
    }

    System.out.println("##############BLOCKS(10 times 1000)##############");
    for(int i = 0; i< repetitions; i++){
      try{
        soloTest("blocks",10);
      }catch(IOException e){
        System.out.print("!!ERROR!!");
        e.printStackTrace();
      }
      System.out.print(newLine);
      peer = peer.reset(); 
      peer2 = peer2.reset();
      
    }
 
  }  
  
  
  /**
   * output format create-prototype-time publish-time create-kb-time publish-time consistency-time fixpoint-time ping
   * @throws IOException
   * @throws InterruptedException 
   */
  private static void soloTest(String type, int fileSize) throws IOException, InterruptedException{
    File file;
    
    
    if(type.equals("baseline")){
      file = FileGenerator.generateBaselineKB(fileSize, randomMutability, shuffle);
    }else if(type.equals("propline")){
      file = FileGenerator.generateProplineKB(fileSize, randomMutability, shuffle); 
    }else if (type.equals("blocks")){
      file = FileGenerator.generateBlockKB(fileSize, randomMutability, shuffle);
    }else if (type.equals("random")){
      file = FileGenerator.generateRandomKB(fileSize, randomMutability, shuffle, true);
    }else{
      throw new Error("data set format");
    }
    
    //create prototype expressions
    Stopwatch w = Stopwatch.createStarted();
    FileInputStream fis = new FileInputStream(file);
    ArrayList<PrototypeReference> res =  peer.putPrototypeExpression(fis, false);
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
    
    fis.close();
    
    //publish prototype
    w = Stopwatch.createStarted();
      peer.publishChanges();
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
    
    //create KB
    w = Stopwatch.createStarted();
      PrototypeKB.Builder kb = new PrototypeKB.Builder(ID.of("kb:test1"));
      for(PrototypeReference ref : res){
        if(Math.random()>0.2){
          kb.add(ref.getImmutableLink());
        }else{
          kb.add(ref.getMutableLink());
        } 
      }
      PrototypeKB pKb = kb.build();
      peer.putKnowledgeBase(pKb, false);   
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS) + " ");
    w.stop();
    
    //publish KB
    w = Stopwatch.createStarted();
      peer.publishChanges();
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
    
    //When Request a Knowledge Base then you need some time until the update has been fully published.
    // since we disabled the IPFS name resolve cache this is no longer a problem! mostly.
     //TimeUnit.MINUTES.sleep(2);
  
    //Request new PrototypeKB 
    w = Stopwatch.createStarted();
    Multihash kbHash = peer2.getKnowledgeBaseHash(peer.getID(),ID.of("kb:test1"));
    FixedPrototypeKB fKb = peer2.getKnowledgeBase(kbHash);
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
    
    //download Prototype Expressions
   w = Stopwatch.createStarted();
   fKb.fix(peer2);
   System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
   w.stop();
    
    //consistency
    w = Stopwatch.createStarted();
      fKb.consistencyCheck();
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
    
    //fixpoint 
    w = Stopwatch.createStarted();
      fKb.computeFixpoint();
    System.out.print(w.elapsed(TimeUnit.MILLISECONDS)+ " ");
    w.stop();
  }
}