package domhues.prototypes.peer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import com.google.common.base.Preconditions;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;

import domhues.prototypes.expressions.IncomingChangeExpression;
import domhues.prototypes.expressions.IncomingPrototypeExpression;
import domhues.prototypes.links.ID;

/**
 *  Several functions which are needed to parse a Prototype Expression File into a list of IncomingPrototypeExpressions.
 * 
 * @author dominikhuser
 *
 */
public class Parser {

  private static HashMap<String, String> marks;
  private static LinkedList<String> list;


  /**
   * Reads a file of prototype expressions and returns a sorted list of incoming prototypes. 
   * The order is optimized for insertion into IPFS. Special Behavior: If an id is not defined inside the file the link keeps part of the referring PE but the not explicitly defined PE won't get an own IncomingPrototype Object.
   * 
   * @param pFile
   * @return A list of Incoming Prototype Expressions in the correct order in which they should published on IPFS.
   */
  public static ArrayList<IncomingPrototypeExpression> getTopologcialOrder(InputStream pFile) throws IOException {
    Preconditions.checkNotNull(pFile);
    HashMap<String, IncomingPrototypeExpression> map = readExpressions(pFile);
    ImmutableGraph<String> graph = computeGraph(map);
    ArrayList<IncomingPrototypeExpression> ret = topologicalOrder(graph, map);
    return ret;
  }

  /**
   * Reads a file of prototypes and returns an unordered map of prototype expressions.
   * 
   * @param pFile
   * @return the HashMap, where the key is the id of a prototype expression. the value is the protype expression itself.
   */
  private static HashMap<String, IncomingPrototypeExpression> readExpressions(InputStream fStream) throws IOException {
    Preconditions.checkNotNull(fStream);
    HashMap<String, IncomingPrototypeExpression> map = new HashMap<String, IncomingPrototypeExpression>();
    BufferedReader fReader = new BufferedReader(new InputStreamReader(fStream));

    String line;
    IncomingPrototypeExpression.Builder proto;
    ID id;
    IncomingChangeExpression base;

    line = fReader.readLine();
    while (line != null) {
      if (line.equals("ID:")) { // Read ID
        line = fReader.readLine();
        if (line != null) {
          if(line.equals("proto:P_0")){
            throw new Error("Proto:P_0 is not allowed in the input file");
          }
          id = ID.of(line);
          line = fReader.readLine();
          if (line != null && line.equals("BASE:")) { // Read BASE
            line = fReader.readLine();
            if (line != null) {
              if(line.equals("proto:ALL")){
                throw new Error("ALL not allowed in base.");
              }
              if (line.contains(" -mutable") && !line.equals("ADD:")) {
                base = new IncomingChangeExpression(ID.of("proto:null"), ID.of(line.substring(0, line.indexOf(" -mutable"))), true);
              } else if(!line.equals("ADD:")){
                base = new IncomingChangeExpression(ID.of("proto:null"), ID.of(line), false);
              }else{
                throw new Error("Prototype out of file misses a base");
              }
              proto = new IncomingPrototypeExpression.Builder(id, base);
              line = fReader.readLine();
              if (line != null && line.equals("ADD:")) {  
                line = fReader.readLine();
                while (line != null && !line.equals("REMOVE:")) { // Read ADD Set
                  if (line.contains(" -mutable")) {
                    if(line.indexOf(" ") == line.indexOf(" -mutable")){
                      throw new Error("wrong file format");
                    }
                    if(line.substring(line.indexOf(" ") + 1, line.indexOf(" -mutable")).equals("proto:ALL")){
                      throw new Error("ALL not allowed in Add statement");
                    }
                    // mutable link
                    proto.add(ID.of(line.substring(0, line.indexOf(" "))), ID.of(line.substring(line.indexOf(" ") + 1, line.indexOf(" -mutable"))), true);
                  } else if(line.contains(" ")){
                    // immutable link
                    if(line.substring(line.indexOf(" ") + 1).equals("proto:ALL")){
                      throw new Error("ALL not allowed in Add statement");
                    }
                    proto.add(ID.of(line.substring(0, line.indexOf(" "))), ID.of(line.substring(line.indexOf(" ") + 1)), false);
                  }else{
                    throw new Error("Wrong Add statment ");
                  }
                  line = fReader.readLine();
                }
                if (line != null && line.equals("REMOVE:")) {
                  // REMOVE
                  line = fReader.readLine();
                  while (line != null && !line.equals("")) { // Read REMOVE Set 
                    if (line.substring(line.indexOf(",")+1).equals("proto:ALL") && line.contains(" ")){
                      // remove all link
                      proto.remove(ID.of(line.substring(0, line.indexOf(" "))), ID.of("proto:ALL"), false);
                    }else if (line.contains(" -mutable")) {
                      // mutable link
                      if(line.indexOf(" ") == line.indexOf(" -mutable")){
                        throw new Error("wrong file format");
                      }
                      proto.remove(ID.of(line.substring(0, line.indexOf(" "))), ID.of(line.substring(line.indexOf(" ") + 1, line.indexOf(" -mutable"))), true);
                    } else if(line.contains(" ")){
                      // immutable link
                      proto.remove(ID.of(line.substring(0, line.indexOf(" "))), ID.of(line.substring(line.indexOf(" ") + 1)), false);
                    }else{
                      throw new Error("Wrong Remove statement.");
                    }
                    line = fReader.readLine();
                  }
                  IncomingPrototypeExpression incProto = proto.build();
                  map.put(incProto.getIdAsString(), incProto);
                }else{
                  throw new Error("REMOVE: is missing in file");
                }
              }else{
                throw new Error("ADD: is missing in file");
              }
            }else{
              throw new Error("Wrong prototype expression format in file.");
            }
          }else{
            throw new Error("Wrong prototype expression format in file.");
          }
        }else{
          throw new Error("Wrong prototype expression format in file.");
        }
      }else{
        throw new Error("Wrong prototype expression format in file.");
      }
      line = fReader.readLine();
    }
    fReader.close();
    fStream.close();

    return map;
  }

  /**
   * Compute the graph representation of a map of prototype expressions.
   * 
   * @param pMap
   * @return Immutable Graph representation.
   */
  private static ImmutableGraph<String> computeGraph(HashMap<String, IncomingPrototypeExpression> pMap) {
    Preconditions.checkNotNull(pMap);
    MutableGraph<String> graph = GraphBuilder.directed().build();

    for (String id : pMap.keySet()) {
      IncomingPrototypeExpression proto = pMap.get(id);
      graph.addNode(proto.getIdAsString());
      graph.putEdge(proto.getIdAsString(), proto.getBase());
      for (IncomingChangeExpression addId : proto.getAddList()) {
        graph.putEdge(proto.getIdAsString(), addId.getLink());
      }
      for (IncomingChangeExpression removeId : proto.getRemoveList()) {
        graph.putEdge(proto.getIdAsString(), removeId.getLink());
      }
    }
   // System.out.println(graph.toString());
    return ImmutableGraph.copyOf(graph);
  }
  
  /**
   * Computes the topological order of the given graph and returns an ordered list of the IncomingPrototypes and not only the ids.
   * 
   * @param graph A graph containing only prototype ids
   * @param pMap A mapping from prototype ids to IncomingPrototypes
   * @return A sorted list of Incoming Prototypes
   */
  public static ArrayList<IncomingPrototypeExpression> topologicalOrder(ImmutableGraph<String> graph, HashMap<String, IncomingPrototypeExpression> pMap) {
    Preconditions.checkNotNull(graph);
    Preconditions.checkNotNull(pMap);
    marks = new HashMap<String, String>(pMap.size());
    list = new LinkedList<String>();
    for (String s : graph.nodes()) {
      marks.put(s, "w");
    }

    for (String s : graph.nodes()) {
      if (marks.get(s).equals("w")) {
        dfsVisit(s, graph, true);
      }
    }

    ArrayList<IncomingPrototypeExpression> result = new ArrayList<IncomingPrototypeExpression>();
    Collections.reverse(list);
    IncomingPrototypeExpression prot;
    for (String s : list) {
      prot = pMap.get(s);
      if (prot != null){ // a link which was not defined in the file
        result.add(prot);
      }else if(!s.equals("proto:P_0") && !s.equals("proto:ALL")){ // proto P_0 is not defined in file. Should not throw an exception.
        throw new Error("URI is not defined in file");
      }
    }   
    return result;
  }
   
  
  /**
   * Detect cycles in a graph. Throws Error if cycle has been found.
   * @param graph
   */
  public static void cycleDetection(ImmutableGraph<String> graph){
    Preconditions.checkNotNull(graph);
    marks = new HashMap<String, String>();
    list = new LinkedList<String>();
    for (String s : graph.nodes()) {
      marks.put(s, "w");
    }

    for (String s : graph.nodes()) {
      if (marks.get(s).equals("w")) {
        dfsVisit(s, graph, true);
      }
    }
  }
  

  /**
   * DFS from a starting node s in Graph pGraph
   * 
   * @param s
   * @param pGraph
   */
  private static void dfsVisit(String s, ImmutableGraph<String> pGraph, boolean cycleDetection) {
    Preconditions.checkNotNull(pGraph);
    Preconditions.checkNotNull(s);
    marks.put(s, "g");
    for (String adj : pGraph.successors(s)) {
      if (marks.get(adj).equals("w")) {
        dfsVisit(adj, pGraph, cycleDetection);
      }else if(cycleDetection && marks.get(adj).equals("g")){
        throw new Error("cycle detected");
      }
    }
    marks.put(s, "b");
    list.addFirst(s);
  }

}
