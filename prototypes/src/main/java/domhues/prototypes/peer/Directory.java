package domhues.prototypes.peer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import io.ipfs.multihash.Multihash;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpnsLink;

/**
 * Administrative Directory of a Peer over which changes to the directory are made and finally published. 
 * @author dominikhuser
 *
 */
public class Directory {
  Multihash topDirHash;
  JSONObject topDir, prototypeDir, knowledgebaseDir;
  int prototypeAmount;
  PeerNode peerNode;
  
  /**
   * Initialize the administrative directory structure.
   * If it is destroyed it will be repaired, whereby data can be lost.
   * @param peer
   * @throws IOException
   */
  public Directory(PeerNode peer) throws IOException{ 
    this.initLocalStorage(peer);
    this.loadFrom(peer);
  }
  
  /**
   * Loads an existing directory of a peer from IPNS.
   * @param peer
   * @throws IOException
   */
  public void loadFrom(PeerNode peer) throws IOException{
    peerNode = peer;
    
    // Request Multihash of dir and fix it if needed
    try {
      // get current dir
      topDirHash = peer.ipfsApi.selfResolve();
      // reconstruct if possible
      peer.ipfsApi.pinAdd(topDirHash, true);
      // in case of no resolution possible, it is likely that there is nothing published yet.
      // critical if resolution did not work but publishing did.
    } catch (Exception e) {
      System.out.println("reconfigured IPNS entry. Non set before, or not all dir objects are available");
      peer.ipfsApi.name.publish(PeerNode.EMPTY);
      topDirHash = PeerNode.EMPTY;
    }
        
    // Request Prototype Dir Object and fix it if it's needed
    try{
      Multihash prototypeDirHash = peer.ipfsApi.getLinkofObject(topDirHash, "prototypes");
      prototypeDir = peer.ipfsApi.getObjectAsJSON(prototypeDirHash);
    }catch(Exception e){
      prototypeDir = new JSONObject().put("Links", new JSONArray()).put("Data", "<empty>");
    }
    
    // Request KB Dir Object and fix it if it's needed
    try{
      Multihash knowledgebaseDirHash = peer.ipfsApi.getLinkofObject(topDirHash, "knowledgebases");
      knowledgebaseDir = peer.ipfsApi.getObjectAsJSON(knowledgebaseDirHash);
    }catch(Exception e){
      knowledgebaseDir = new JSONObject().put("Links", new JSONArray()).put("Data", "<empty>");
    }
    
    // get amount of prototype expressions
    prototypeAmount =  ((JSONArray)prototypeDir.get("Links")).length();
    
  }
  
  /**
   * Init the local storage of the peer.
   * Pin EMPTY, ALL and proto:P_0 on the node if not yet done.
   * @param peer
   * @throws IOException 
   */
  private void initLocalStorage(PeerNode peer) throws IOException{
    // Create and pin an empty object
    peer.ipfsApi.newObject();
    peer.ipfsApi.pinAdd(PeerNode.EMPTY, true);
    
    // Create and pin proto:ALL object
    peer.ipfsApi.patchSetData(PeerNode.EMPTY, "proto:ALL");
    peer.ipfsApi.pinAdd(PeerNode.ALL,true);

    // Create and pin proto:P_0
    Multihash add = peer.ipfsApi.patchSetData(PeerNode.EMPTY, "proto:ADD");
    Multihash rem = peer.ipfsApi.patchSetData(PeerNode.EMPTY, "proto:REMOVE");
    Multihash proto0 = peer.ipfsApi.patchSetData(PeerNode.EMPTY, "proto:P_0");
    proto0 = peer.ipfsApi.patchAddLink(proto0, "add", add);
    proto0 = peer.ipfsApi.patchAddLink(proto0, "remove", rem);
    peer.ipfsApi.pinAdd(proto0, true);  
  }
  
  /**
   * Publish the changes which were made to the Directory.
   * @param peer
   * @throws IOException
   */
  public void publish() throws IOException{
    
    //put the new knowledgebase dir on IPFS
    Multihash knowledgebaseDirHash= peerNode.ipfsApi.putObjectAsJSON(knowledgebaseDir);
    
    //put the new prototype dir on IPFS
    Multihash prototypeDirHash = peerNode.ipfsApi.putObjectAsJSON(prototypeDir);
    
    //create new top dir
    JSONObject prototypeLink = new JSONObject().put("Name", "prototypes").put("Hash", prototypeDirHash.toBase58());
    JSONObject knowledgebaseLink = new JSONObject().put("Name", "knowledgebases").put("Hash", knowledgebaseDirHash.toBase58());
    topDir = new JSONObject().put("Data", "<empty>").put("Links", new JSONArray().put(prototypeLink).put(knowledgebaseLink));
    
    //put new topDir on IPFS
    Multihash newDirHash = peerNode.ipfsApi.putObjectAsJSON(topDir);
        
    //pin new Dir, unpin unused objects
    peerNode.ipfsApi.pinAdd(newDirHash, true);
    peerNode.ipfsApi.pinRm(topDirHash, true);    
    peerNode.ipfsApi.name.publish(newDirHash);
    
    // update current hash
    topDirHash = newDirHash;
  }
  
  /**
   * Publish the changes which were made to the Directory.
   * Deletes all not need Prototype Expressions (GC call).
   * @throws IOException
   */
  public void publishAndClean() throws IOException{
    publish();
    peerNode.ipfsApi.repo.gc();
  }
   
  
  /**
   * Add a Prototype Expression from the directory (No publishing).
   * @param prototype expression's hash
   * @return Ipns Link of the added Object. Remind that this links is not yet resolvable until publish() has been called.
   */
  public IpnsLink addPrototypeExpression(Multihash pe){
    JSONObject newPELink = new JSONObject().put("Name", Integer.toString(prototypeAmount)).put("Hash", pe.toBase58());
    ((JSONArray)prototypeDir.get("Links")).put(newPELink);  
    IpnsLink mutableLink = new IpnsLink("/ipns/"+peerNode.getID()+"/prototypes/"+prototypeAmount);
    prototypeAmount++;
    return mutableLink;
  }
  
  /**
   * remove a Prototype Expression from the directory (No publishing).
   * The mutable link will point to proto:P_0 right after.
   * @param prototype expression's hash
   */
  public void removePrototypeExpression(Multihash pe){
  //search correct object and delete it 
    JSONArray array = prototypeDir.getJSONArray("Links");
    for (int i = 0; i < array.length(); i++) {
      if (array.getJSONObject(i).getString("Hash").equals(pe.toBase58())) {
        replacePrototypeExpression(i, PeerNode.PROTO0);
      }
    }
  }
  
  /**
   * Replace a Prototype expression with index i (/ipns/prototypes/i) with the replacer.
   * @param index
   * @param replacer
   */
  public void replacePrototypeExpression(int index, Multihash replacer){
    JSONArray array = prototypeDir.getJSONArray("Links");
    array.remove(index);
    JSONObject placeHolderLink = new JSONObject().put("Name", Integer.toString(index)).put("Hash",  replacer.toBase58());
    array.put(placeHolderLink);    
  }
  
  /**
   * Add knowledge base to the directory. 
   * @param kb Mutlihash of the KB.
   * @param name name of the KB.
   * @throws JSONException
   * @throws UnsupportedEncodingException
   */
  public void addKnowledgeBase(Multihash kb, ID name) throws JSONException, UnsupportedEncodingException{
    JSONObject kbLink = new JSONObject().put("Name", ID.encodeUtf8(name)).put("Hash", kb);
    removeKnowledgeBase(name);
    knowledgebaseDir.getJSONArray("Links").put(kbLink);
  }
  
  /**
   * remove a KB from the directory (No publishing).
   * @param name ID of the KB which should be removed.
   */
  public void removeKnowledgeBase(ID name) throws UnsupportedEncodingException{
    JSONArray array = knowledgebaseDir.getJSONArray("Links");
    String encoding = ID.encodeUtf8(name);
    for (int i = 0; i < array.length(); i++) {
      if (array.getJSONObject(i).getString("Name").equals(encoding)) {
        array.remove(i);
      }
    }
  }
  
  
  /**
   * Amount of prototype expressions on the node. Note that removed links are included since they now point to Proto:P_0.
   * @return amount of prototype expressions.
   */
  public int countPrototypeExpressions(){
    return prototypeAmount;
  }

}
