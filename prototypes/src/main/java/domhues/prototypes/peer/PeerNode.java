package domhues.prototypes.peer;

import io.ipfs.multihash.Multihash;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.google.common.base.Preconditions;

import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.knowledgebase.FixedPrototypeKB;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.IpnsLink;
import domhues.prototypes.links.Link;
import domhues.prototypes.links.PrototypeReference;

/**
 * ProtoIPFS User Interface.
 * 
 * @author dominikhuser
 *
 */
public class PeerNode {

  // Properties of a peer node
  private String host;
  private int port;
  public IpfsApi ipfsApi;
  Directory directory;
  
  PeerNodeGet get;
  PeerNodePut put;
  
  // Often used hashes
  public static final Multihash PROTO0 = Multihash.fromBase58("QmSCL4BD3T138XioiVuMRNqRTTaSJ1ytRYhce5urDco84p"); // IPFS object containing only data "proto:P_0"
  static final Multihash EMPTY = Multihash.fromBase58("QmdfTbBqBPQ7VNxZEYEj14VmRuZBkqFbiwReogJgS1zR1n"); // empty IPFS object
  public static final Multihash ALL = Multihash.fromBase58("Qmakh9kr9ptzJ2wxqDkKhaiVF9PUdKuqYgUirjmbEVN5M5"); // proto All
  
  
  /**
   * Creates a new interface to the already running IPFS daemon at pHost#pPort.
   * 
   * @param pHost Host of the daemon as IPv4 address
   * @param pPort Port of the daemon
   */
  public PeerNode(String pHost, int pPort, Multihash pPeerID) throws IOException {
    Preconditions.checkNotNull(pHost);
    Preconditions.checkNotNull(pPort);
    Preconditions.checkNotNull(pPeerID);

    ipfsApi = new IpfsApi(pHost, pPort, pPeerID);
    port = pPort;
    host = pHost;
    
    directory = new Directory(this);
    get = new PeerNodeGet(this);
    put = new PeerNodePut(this);
  }

  /**
   * Get a the address of the pKB named knowledge base at a specific peer.
   * 
   * @param pPeerId IPFS peer id of the user which is hosting the knowledge base
   * @param pKBName name of the knowledge base, which is unique at one peer.
   * @return The hash of the IPFS object, which is representing this knowledge base
   */
  public Multihash getKnowledgeBaseHash(Multihash pPeerId, ID pKBName) throws IOException {
    return get.knowledgeBaseHash(pPeerId, pKBName);
  }

  /**
   * Get a the address of the pKB named knowledge base at your own peer.
   * 
   * @param pKBName name of the knowledge base, which is unique at one peer.
   * @return Multihash. The hash of the IPFS object, which is representing this knowledge base.
   */
  public Multihash getKnowledgeBaseHash(ID pKBName) throws IOException {
    return get.knowledgeBaseHash(pKBName);
  }

  /**
   * Get a knowledge base by its IPFS object hash.
   * 
   * @param pLink Multihash of the knowledge base. Could be gathered by getKnwoledgeBaseHash.
   * @return PrototypeKB. The prototype knowledge base object.
   */
  public FixedPrototypeKB getKnowledgeBase(Multihash pLink) throws IOException {
    return get.knowledgeBase(pLink);
  }

  /**
   * Get a already published Prototype Expression by a Link.
   * 
   * @param pLink a Ipfs or Ipns Link.
   * @param pImmutable determines whether all links in the resulting publishedPrototypeExpression shoud be immutable. Mutable links will be converted to mutable links.
   * @return PublishedPrototyepExpression.
   */
  public PublishedPrototypeExpression getPrototypeExpression(Link pLink, boolean pImmutable) throws IOException {
    return get.prototypeExpression(pLink, pImmutable);
  }
  
  /**
   * Constructs a IPFS representation for a set of prototype expressions encoded in a file with the following format: 
   * ID: <Prototype Expression ID id> BASE: <Prototype Expression ID base> [-mutable] ADD: <Link name>,<Prototype Expression ID add 1> [-mutable] <...> <Link name>,<Prototype Expression ID add n> [-mutable] REMOVE: <Link name>,<Prototype Expression ID rem 1> [-mutable] <...> <Link name>,<Prototype Expression ID rem m> [-mutable] Note that the output list does not contain proto:P_0 even if it was defined in the file. TODO sinnvoll? ID: <...>
   * 
   * All links in this file only can refer to prototype expressions which are defined in the same file as well. 
   * To construct a prototype expression which refers to already published prototype expressions use publishPrototype(PrototypeExpression pProto, boolean pUpdateDir).
   * Pins all recursively used prototype expressions in your node.
   * @param pFile with the mentioned format.
   * @param pUpdateDir defines whether the constructed IPFS representation should be published on IPNS instantly (true), or whether it should be published with further prototype expressions manually, later (false).
   * @return an array of PrototypeReferences. For every constructed IPFS representation this array holds a Prototype Reference. This reference contains the mutable link and the immutable link of this prototype expression.
   */
  public ArrayList<PrototypeReference> putPrototypeExpression(InputStream pFile, boolean pUpdateDir) throws IOException {
    return put.prototypeExpression(pFile, pUpdateDir);
  }
  
  /**
   * Look at publishPrototype. Accepts file as input.
   * 
   * @param pFile
   * @param pUpdateDir
   * @return
   * @throws IOException
   */
  public ArrayList<PrototypeReference> putPrototypeExpression(File pFile, boolean pUpdateDir) throws IOException {
    FileInputStream fStream = new FileInputStream(pFile);
    return putPrototypeExpression(fStream, pUpdateDir);
  }

  /**
   * @deprecated
   * Constructs an IPFS representation of a Prototype Expression. Uses PATCH Object approach.
   * @param pProto
   * @return A PrototypeReference. This reference contains the mutable link and the immutable link of this prototype expression.
   * @throws IOException
   */
  public PrototypeReference putPrototypeExpressionDepricated(PrototypeExpression pProto, boolean pUpdateDir) throws IOException {
    return put.prototypeExpressionDepricated(pProto, pUpdateDir);
  }
  
  /**
   * Alternative way to publish a prototype expression via put.  
   * @param pProto
   * @param pUpdateDir
   * @return
   * @throws IOException
   */
  public PrototypeReference putPrototypeExpression(PrototypeExpression pProto, boolean pUpdateDir) throws IOException {
    return put.prototypeExpression(pProto, pUpdateDir);
  }
  
  /**
   * Publishes a knowledge base on your own node. Use self constructed prototype expressions or protype expressions from other nodes. Prototype expressions from other nodes will be pinned to your own node as long as it is part of your knowledge base
   * 
   * @param pKB a PrototypeKB object.
   * @param pUpdateDir defines whether the constructed IPFS representation should be published on IPNS instantly (true),
   * @return the hash of the Ipfs representation.
   * @throws IOException
   */
  public Multihash putKnowledgeBase(PrototypeKB pKB, boolean pUpdateDir) throws IOException {
    return put.knowledgeBase(pKB, pUpdateDir);
  }
 
  /**
   * Depublish a prototype expression via a prototype expression. Does not care whether it is used in a knowledge base or in a property. Unpins all linked prototype expressions as long as they are not explicitly (IPFS link not IPNS link) used in one of your knowledge bases or were published via publishPrototypeExpression. A prototype expression can be depublished even if it is part of a knowledge base of yours. Check this first. Mutable links will be destroyed.
   * 
   * @param pProto
   * @throws IOException
   */
  public void removePrototypeExpression(PublishedPrototypeExpression pProto, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(pProto);
    IpfsLink link = pProto.getIpfsLink();
    removePrototypeExpression(link, pUpdateDir);
  }

  /**
   * Depublish a prototype expression via a given link. Does not care whether it is used in a knowledge base or in a property. Unpins all linked prototype expressions as long as they are not explicitly (IPFS link not IPNS link) used in one of your knowledge bases or were published via publishPrototypeExpression. A prototype expression can be depublished even if it is part of a knowledge base of yours. Check this first. Mutable links will be destroyed.
   * 
   * @param pLink
   * @throws IOException
   */
  public void removePrototypeExpression(Link pLink, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(pLink);
    if(pLink instanceof IpfsLink){
      // /ipfs/hash
      Link.checkLinkLength(pLink, 2);
    }else if(pLink instanceof IpnsLink){
      // /ipns/node-id/prototype/count
      Link.checkLinkLength(pLink, 4);
    }else{
      throw new IOException("wrong Link type");
    }
    Multihash hash = ipfsApi.resolveLink(pLink);
    removePrototypeExpression(hash, pUpdateDir);

  }

  /**
   * Depublish a prototype expression via a given hash. Does not care whether it is used in a knowledge base or in a property. Unpins all linked prototype expressions as long as they are not explicitly (IPFS link not IPNS link) used in one of your knowledge bases or were published via publishPrototypeExpression. A prototype expression can be depublished even if it is part of a knowledge base of yours. Check this first. Mutable links will be destroyed.
   * 
   * @param pLink
   * @throws IOException
   */
  public void removePrototypeExpression(Multihash pHash, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(pHash);
    Preconditions.checkNotNull(pUpdateDir);
  
    directory.removePrototypeExpression(pHash);      
    //update Dir if wanted
    if(pUpdateDir){
      directory.publish();
    }


  }

  /**
   * Depublish a knowledge base given the KB name. Unpins all prototype expressions which are not published in this node explicitly (via publishPrototypeExpression) If a prototype expression is used in an other knowledge base, then it won't be unpinned. Unpins the knowledgebase as well. Deleted from local node after calling ipfs garbage collection
   * 
   * @param name of the KB
   * @param pUpdateDir directly updating the IPNS Dir.
   * @throws IOException
   */
  public void removeKnowledgeBase(ID name, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(name);
    Preconditions.checkNotNull(pUpdateDir);   
    directory.removeKnowledgeBase(name);
    if (pUpdateDir) {
      publishChanges();
    }
  }
  
  /**
   * Publishes commitments which were done after the last call of this method (may be indirectly by setting flags in change methods) To add or remove something from the IPNS directory use: addPrototypeExpressionToDir, depublishPrototype
   * 
   * @throws IOException
   */
  public void publishChanges() throws IOException {
    directory.publish();
  }
  
  /**
   * Updates link of your own node. If you use hash of a different node which is not yours, this method tries to update the link on your node, if there is a prototype expression mutable refered to with <int>.
   * @param pLink must be the form /ipns//<int>
   * @param pTarget the new object which this link should refer to
   * @param pUpdateDir
   * @throws IOException
   */
  public void updateMutableLink(IpnsLink pLink, Link pTarget, boolean pUpdateDir) throws IOException{
    // case not own node
    Preconditions.checkNotNull(pLink);
    Preconditions.checkNotNull(pTarget);
    
    if(pTarget instanceof IpfsLink){
      // /ipfs/hash
      Link.checkLinkLength(pTarget, 2);
    }else if(pTarget instanceof IpnsLink){
      // /ipns/node-id/prototype/count
      Link.checkLinkLength(pTarget, 4);
    }else{
      throw new IOException("wrong Link type");
    }
    
    Link.checkLinkLength(pLink, 4);
    
    String link = Link.deleteFirstSegment(pLink.toString());
    //check if own node TODO What if you use the multihash of your node
    if(!Link.getFirstSegment(link).equals("") && !Link.getFirstSegment(link).equals(ipfsApi.getPeerID().toBase58())){
      throw new Error("You can not change the dir of someone elses node.");
    }    
    link = Link.deleteFirstSegment(link);
    
    //check if it is refering to a prototype
    if(!Link.getFirstSegment(link).equals("prototypes")){
      throw new Error("Can not change a mutable link which is not referring to a PE.");
    }
    link = Link.deleteFirstSegment(link);
    
    String num  = Link.getFirstSegment(link);
    
    link = Link.deleteFirstSegment(link);
     
    int index = Integer.valueOf(num);
    //checks if number is higher than the amount prototypes 
    if(index >= directory.countPrototypeExpressions()){
      throw new Error("Links Index is to high. PE not found.");
    }
    
    // replace
    Multihash replacer = ipfsApi.resolveLink(pTarget);
    directory.replacePrototypeExpression(index, replacer);
    
    //update in needed 
    if(pUpdateDir){
      publishChanges();
    }
  }
  
  /**
   * unpin and removes every prototype expression and every IPFS object and reset the IPNS directory to EMPTY.
   * @return 
   * @throws IOException
   */
  public PeerNode reset() throws IOException{   
    //unpin everything
    for(Multihash hash : ipfsApi.pinls("recursive")){
      try{
        ipfsApi.pinRm(hash, true);
      }catch(IOException e){
        // already recursively removed or only indirectly pinned.
      }
    }
    
    //garbage collection
    ipfsApi.repo.gc();

    // reset IPNS
    ipfsApi.name.publish(EMPTY);
    
    //initialize new version 
    return new PeerNode(host, port, this.getID());   
  }
    
  public Multihash getID(){
    return ipfsApi.getPeerID();
  }
  
}
