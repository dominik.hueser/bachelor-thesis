package domhues.prototypes.peer;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Preconditions;

import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.IpnsLink;
import domhues.prototypes.links.Link;

/**
 * Extended Java IPFS API. 
 * 
 * @author dominikhuser
 *
 */
public class IpfsApi extends IPFS {
  Multihash peerID;
  
  /**
   * Create a new IpfsAPI instance and bind it to a running IPFS daemon at IP pHost, port pPort.
   * @param pHost
   * @param pPort
   */
  public IpfsApi(String pHost, int pPort, Multihash pPeerID) {
    super(pHost, pPort);
    peerID = pPeerID;
  }

  
  /**
   * Get the JSON String representation of an IPFS object. Replacement for IPFS.object.get, since it does not give the link names.
   * 
   * @param pHash Hash of the IPFS object.
   * @return the JSON String representation of the IPFS object
   * @throws IOException
   */  
  public JSONObject getObjectAsJSON(Multihash pHash) throws IOException {
    Preconditions.checkNotNull(pHash);
    URL link = new URL("http://" + host + ":" + port + "/api/v0/object/get?arg=" + pHash.toBase58());
    HttpURLConnection yc = (HttpURLConnection) link.openConnection();
    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
    String inputLine = in.readLine();
    in.close();
    yc.disconnect();
    return new JSONObject(inputLine);
  }
  
  /**
   * put an IPFS object encoded in JSON.
   * @param obj
   * @return
   * @throws IOException
   */
  public Multihash putObjectAsJSON(JSONObject obj) throws IOException{
    ArrayList<byte[]> list = new ArrayList<byte[]>();
    list.add(obj.toString().getBytes());
    return object.put(list).get(0).hash;
  }

  /**
   * Pin pHash in IPFS such that the next garbage collection deletes this from local storage.
   * 
   * @param pHash Hash of the object.
   * @param pRecursive pin referred objects as well.
   * @return
   * @throws IOException
   */
  public void pinAdd(Multihash pHash, boolean pRecursive) throws IOException {
    Preconditions.checkNotNull(pHash);
    Preconditions.checkNotNull(pRecursive);  
    URL link = new URL("http://" + host + ":" + port + "/api/v0/pin/add?arg=" + pHash.toBase58() + "&recursive=" + pRecursive);
    HttpURLConnection yc =(HttpURLConnection) link.openConnection();
    yc.setReadTimeout(10000);
    yc.disconnect();
  }

  /**
   * Unpin pHash in IPFS such that the next garbage collection deletes this from local storage.
   * 
   * @param pHash Hash of the object.
   * @param pRecursive unpin referred objects as well.
   * @return
   * @throws IOException
   */
  public void pinRm(Multihash pHash, boolean pRecursive) throws IOException {
    Preconditions.checkNotNull(pHash);
    Preconditions.checkNotNull(pRecursive);    
    URL link = new URL("http://" + host + ":" + port + "/api/v0/pin/rm?arg=" + pHash.toBase58() + "&recursive=" + pRecursive);
    HttpURLConnection yc =(HttpURLConnection) link.openConnection();
    yc.setReadTimeout(10000);
    yc.disconnect();
  }

  /**
   * Finds the hash of an object which is referred to via pLinkName in pObject.
   * 
   * @param pObject
   * @param pLinkName
   * @return Multihash
   * @throws IOException if link name was not found
   */
  public Multihash getLinkofObject(Multihash pObject, String pLinkName) throws IOException {
    Preconditions.checkNotNull(pObject);
    Preconditions.checkNotNull(pLinkName);
    JSONObject o = getObjectAsJSON(pObject);
    // get array of links
    JSONArray array = o.getJSONArray("Links");

    for (int i = 0; i < array.length(); i++) {
      if (array.getJSONObject(i).get("Name").toString().equals(URLEncoder.encode(pLinkName, "UTF-8"))) {
        return (Multihash) Multihash.fromBase58(array.getJSONObject(i).get("Hash").toString());
      }
    }
    throw new IOException("link name not found.");
  }

  /**
   * patches an IPFS objects data.Previous data will be replaced.
   * 
   * @param pObject which should be patched
   * @param pData which should be set
   * @return hash of the newly constructed IPFS object
   * @throws IOException
   */
  public Multihash patchSetData(Multihash pObject, String pData) throws IOException {
    Preconditions.checkNotNull(pObject);
    Preconditions.checkNotNull(pData);
    pObject = (Multihash) object.patch(pObject, "set-data", Optional.of(pData.getBytes()), Optional.<String>empty(), Optional.<Multihash>empty()).hash;
    return pObject;
  }

  /**
   * patches an IPFS objects by adding a link.
   * 
   * @param pObject which should be patched.
   * @param pName name of the link.
   * @param pTarget target of the link.
   * @return hash of the newly constructed IPFS object.
   * @throws IOException
   */
  public Multihash patchAddLink(Multihash pObject, String pName, Multihash pTarget) throws IOException {
    Preconditions.checkNotNull(pObject);  
    Preconditions.checkNotNull(pName);
    Preconditions.checkNotNull(pTarget);
    pObject = (Multihash) object.patch(pObject, "add-link", Optional.<byte[]>empty(), Optional.of(URLEncoder.encode(pName,"UTF-8")), Optional.of((Multihash) pTarget)).hash;
    return pObject;
  }

  /**
   * patches an IPFS objects by removing a link.
   * 
   * @param pObject
   * @param pName which should be patched.
   * @param pReplace
   * @return true if the previous version should be unpinned.
   * @throws IOException
   */
  public Multihash patchRmLink(Multihash pObject, String pName) throws IOException {
    Preconditions.checkNotNull(pObject);
    Preconditions.checkNotNull(pName);
    pObject = (Multihash) object.patch(pObject, "rm-link", Optional.<byte[]>empty(), Optional.of(URLEncoder.encode(pName,"UTF-8")), Optional.<Multihash>empty()).hash;
    return pObject;
  }

  /**
   * Resolves the own peer node. Returns the hash which is saved in the IPNS of your own peer node.
   * 
   * @return hash which is pinned in your node, by default EMPTY
   * @throws IOException
   */
  public Multihash selfResolve() throws IOException {
    URL link = new URL("http://" + host + ":" + port + "/api/v0/name/resolve");
    HttpURLConnection yc = (HttpURLConnection) link.openConnection();
    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

    String inputLine = in.readLine();
    in.close();
    yc.disconnect();
    JSONObject jsonEncoding = new JSONObject(inputLine);
    inputLine = jsonEncoding.getString("Path");
    inputLine = Link.deleteFirstSegment(inputLine);
    inputLine = inputLine.substring(1);
    return Multihash.fromBase58(inputLine);

  }

  /**
   * Create a new IPFS object. returns the hash.
   * @return Mutihash of the new object.
   * @throws IOException
   */
  public Multihash newObject() throws IOException {
    return object._new(Optional.<String>empty()).hash;
  }
  
  
  /**
   * lists all pinned hashes which satisfy the given type. 
   * @param type, direct, recursive, indirect
   * @return ArrayList of Multihashes.
   * @throws IOException
   */
  public ArrayList<Multihash> pinls(String type) throws IOException{
    URL link = new URL("http://" + host + ":" + port + "/api/v0/pin/ls?type="+type);
    HttpURLConnection yc = (HttpURLConnection) link.openConnection();
    yc.setReadTimeout(10000);
    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
    String inputLine = in.readLine();
    in.close();
    yc.disconnect();
    JSONObject jsonEncoding = new JSONObject(inputLine);
    String[] tmp = JSONObject.getNames(jsonEncoding.getJSONObject("Keys"));
    ArrayList<Multihash> res = new ArrayList<Multihash>();
    //in case of empty Keys set the methode returns null.
    if(tmp == null){
      return res;
    }
    for(String s : tmp){
      res.add(Multihash.fromBase58(s));
    }
    return res;
  }
  
  
  /**
   * Resolves a Link and return the has of the links target IPFS object.
   * An IPNS object will be resolved first and after that it is treated like an IPFS link 
   * @param pLink The link to resolve (IPNS or IPFS).
   * @return Multihash of links's target
   * @throws IOException
   */
  public Multihash resolveLink(Link pLink) throws IOException{
    Preconditions.checkNotNull(pLink);
    String sLink = Link.deleteFirstSegment(pLink.toString());
    
    // resolve an IPNS link and create an IPFS link instead.
    if (pLink instanceof IpnsLink) {
      Multihash objectHash;
      if (Link.getFirstSegment(sLink).equals("")) {
        objectHash = selfResolve();
        pLink = new IpfsLink("/ipfs/"+objectHash.toBase58()+ Link.deleteFirstSegment(sLink));
      } else {
        //We want to allow fast updates, therfore we don't cache old ipns resolutions
        String url = "http://" + host + ":" + port + "/api/v0/name/resolve?arg="+Link.getFirstSegment(sLink)+"&nocache=true";
        URL link = new URL(url);
        HttpURLConnection yc = (HttpURLConnection) link.openConnection();
        //yc.setReadTimeout(40000);
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        String inputLine = in.readLine();
        in.close();
        yc.disconnect();
        JSONObject jsonEncoding = new JSONObject(inputLine);
        String resolve = jsonEncoding.get("Path").toString(); 
       // System.out.println(tmp);
        resolve = resolve.substring(6, resolve.length());
        objectHash = (Multihash) Multihash.fromBase58(resolve);
        pLink = new IpfsLink("/ipfs/"+objectHash+"/"+Link.deleteFirstSegment(sLink));
       
      }
    }
    
    // case of /ipfs/<hash>, then no API call needed since Multihash already inside the link.
    String l = pLink.toString();
    if(l.length() - l.replace("/", "").length()==2){
      return Multihash.fromBase58(l.substring(6));
    }
      
    //resolve IPFS link
    String url = "http://" + host + ":" + port + "/api/v0/resolve?arg="+URLEncoder.encode(pLink.toString(),"UTF-8");
    URL link = new URL(url);
    HttpURLConnection yc = (HttpURLConnection) link.openConnection();
    // No timeout set here since no guarantee when response will arrive.
    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
    String inputLine = in.readLine();
    in.close();
    yc.disconnect();
    JSONObject jsonEncoding = new JSONObject(inputLine);
    String tmp = jsonEncoding.get("Path").toString();
    tmp = tmp.substring(6);
    return Multihash.fromBase58(tmp);  
  } 
  
  /**
   * Get Peer ID
   * @return Mutlihash peer ID.
   */
  public Multihash getPeerID(){
    return peerID;
  }
}
