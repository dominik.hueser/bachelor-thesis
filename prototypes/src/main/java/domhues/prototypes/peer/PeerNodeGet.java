package domhues.prototypes.peer;

import io.ipfs.multihash.Multihash;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Preconditions;

import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.knowledgebase.FixedPrototypeKB;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.IpnsLink;
import domhues.prototypes.links.Link;

/**
 * This class contains all get operations which the peer node offers.
 * 
 * @author dominikhuser
 *
 */
class PeerNodeGet {
  PeerNode peer;
  IpfsApi ipfsApi;
  
  public PeerNodeGet(PeerNode pPeer){
    peer = pPeer;
    ipfsApi = peer.ipfsApi;
  }
  
  /**
   * Get a the address of the pKB named knowledge base at a specific peer.
   * 
   * @param pPeerId IPFS peer id of the user which is hosting the knowledge base
   * @param pKBName name of the knowledge base, which is unique at one peer.
   * @return The hash of the IPFS object, which is representing this knowledge base
   */
  public Multihash knowledgeBaseHash(Multihash pPeerId, ID pKBName) throws IOException {
    Preconditions.checkNotNull(pPeerId);
    Preconditions.checkNotNull(pKBName);
    IpnsLink link = new IpnsLink("/ipns/" + pPeerId.toBase58() + "/knowledgebases/" + ID.encodeUtf8(pKBName));
    return ipfsApi.resolveLink(link);
  }

  /**
   * Get a the address of the pKB named knowledge base at your own peer.
   * 
   * @param pKBName name of the knowledge base, which is unique at one peer.
   * @return Multihash. The hash of the IPFS object, which is representing this knowledge base.
   */
  public Multihash knowledgeBaseHash(ID pKBName) throws IOException {
    Preconditions.checkNotNull(pKBName);
    IpnsLink link = new IpnsLink("/ipns//knowledgebases/" + ID.encodeUtf8(pKBName));
    return ipfsApi.resolveLink(link);
  }

  /**
   * Get a knowledge base by its IPFS object hash.
   * 
   * @param pLink Multihash of the knowledge base. Could be gathered by getKnwoledgeBaseHash.
   * @return PrototypeKB. The prototype knowledge base object.
   */
  public FixedPrototypeKB knowledgeBase(Multihash pLink) throws IOException {
    Preconditions.checkNotNull(pLink);
    IpfsLink ipfsLink;
    IpnsLink ipnsLink;
    
    JSONObject jsonEncoding = ipfsApi.getObjectAsJSON(pLink);

    ID name = ID.of(jsonEncoding.getString("Data"));
    PrototypeKB.Builder kb = new PrototypeKB.Builder(name);

    JSONArray array = jsonEncoding.getJSONArray("Links");
    for (int i = 0; i < array.length(); i++) {
      if (array.getJSONObject(i).getString("Name").contains("-mutable")) {
        ipfsLink = new IpfsLink("/ipfs/" + array.getJSONObject(i).getString("Hash"));
        JSONObject ipnsTunnel = ipfsApi.getObjectAsJSON(ipfsApi.resolveLink(ipfsLink));
        String ipnsString = ipnsTunnel.getString("Data");
        ipnsString = ipnsString.substring(ipnsString.indexOf("(")+1, ipnsString.indexOf(")"));
        ipnsLink = new IpnsLink(ipnsString);
        kb.add(ipnsLink);
      } else {
        ipfsLink = new IpfsLink("/ipfs/" + array.getJSONObject(i).getString("Hash"));
        kb.add(ipfsLink);
      }
    }
    PrototypeKB res = kb.build();
    return res.fix(peer);
  }

  /**
   * Get a already published Prototype Expression by a Link.
   * 
   * @param pLink a Ipfs or Ipns Link.
   * @param pImmutable determines whether all links in the resulting publishedPrototypeExpression shoud be immutable. Mutable links will be converted to mutable links.
   * @return PublishedPrototyepExpression.
   */
  public PublishedPrototypeExpression prototypeExpression(Link pLink, boolean pImmutable) throws IOException {
    Preconditions.checkNotNull(pLink);
    
    if(pLink instanceof IpfsLink){
      // /ipfs/hash
      Link.checkLinkLength(pLink, 2);
    }else if(pLink instanceof IpnsLink){
      // /ipns/node-id/prototype/count
      Link.checkLinkLength(pLink, 4);
    }else{
      throw new IOException("wrong Link type");
    }
    
    Multihash hash = ipfsApi.resolveLink(pLink);
    JSONObject jsonRepresentation = ipfsApi.getObjectAsJSON(hash);
    String id = jsonRepresentation.getString("Data");
    JSONArray links = jsonRepresentation.getJSONArray("Links");
    
    JSONObject temp;
    Multihash base, addSet, removeSet;
    base = null;
    addSet = null;
    removeSet = null;

    for (int i = 0; i < links.length(); i++) {
      temp = links.getJSONObject(i);
      if (temp.get("Name").equals("base")) {
        base = Multihash.fromBase58(temp.getString("Hash"));
      } else if (temp.get("Name").equals("add")) {
        addSet = Multihash.fromBase58(temp.getString("Hash"));
      } else if (temp.get("Name").equals("remove")) {
        removeSet = Multihash.fromBase58(temp.getString("Hash"));
      } else {
        throw new IOException("IPFS prototype expression structure failure. Uncommon links found.");
      }
    }

    if (id.equals("proto:P_0")) {
      return new PublishedPrototypeExpression.Builder(ID.of("proto:P_0")).addHash(PeerNode.PROTO0).build();
    }

    // Check if everything is set.
    if ((base == null || removeSet == null || addSet == null)) {
      throw new IOException("IPFS prototype expression structure failure. Uncommon links found. " + pLink.toString());
    }

    // Extract Base:
    Link baseLink = getPrototypeExpressionBase(base, pImmutable);

    PublishedPrototypeExpression.Builder builder = (new PublishedPrototypeExpression.Builder(ID.of(id))).addHash(hash).setBase(baseLink);

    // Extract AddList:
    builder =  getPrototypeExpressionsAddList(builder, addSet, pImmutable);

    // Extract RemoveList:
    builder = getPrototypeExpressionsRemoveList(builder, removeSet, pImmutable);

    return builder.build();

  }
  
  /**
   * get the base link.
   * @param base multihash of the base. 
   * @param pImmutable create an immutable Prototype Expression
   * @return Base Link (might be IPFS or IPNS)
   * @throws IOException
   */
  private Link getPrototypeExpressionBase(Multihash base, boolean pImmutable) throws IOException{
    // Extract Base:
    JSONObject jsonRepresentation = ipfsApi.getObjectAsJSON(base);
    Link baseLink = null;
    String data = "";
    if (jsonRepresentation.getString("Data").contains("proto:IPNSTUNNEL")) {
      // Mutable base link
      data = jsonRepresentation.getString("Data");
      baseLink = new IpnsLink(data.substring(data.indexOf("(") + 1, data.indexOf(")")));
      if (pImmutable) {
        baseLink = new IpfsLink("/ipfs/" + ipfsApi.resolveLink(baseLink));
      }
    return baseLink;       
    } else {
      // Immtable base link
      return Link.fromHash(base);
    }
  }
  
  /**
   * get IPFS Add Change Set from the IPFS prototype Expression and add it to the builder.
   * @param builder, which should be extended 
   * @param addSet, multihash to the add set
   * @param pImmutable, make an immutable prototype expression.
   * @return extended builder builder
   * @throws IOException
   */
  private PublishedPrototypeExpression.Builder getPrototypeExpressionsAddList(PublishedPrototypeExpression.Builder builder, Multihash addSet, boolean pImmutable) throws IOException{
    JSONArray severalLinks, links;
    String data;
    Multihash targetLink, severalLink;
    JSONObject jsonRepresentation = ipfsApi.getObjectAsJSON(addSet);
    if (jsonRepresentation.getString("Data").equals("proto:ADD")) {
      links = jsonRepresentation.getJSONArray("Links");
      for (int i = 0; i < links.length(); i++) {
        severalLink = Multihash.fromBase58(links.getJSONObject(i).getString("Hash"));
        jsonRepresentation = ipfsApi.getObjectAsJSON(severalLink);
        if (jsonRepresentation.getString("Data").equals("proto:SEVERAL")) {
          severalLinks = jsonRepresentation.getJSONArray("Links");
          for (int j = 0; j < severalLinks.length(); j++) {
            targetLink = Multihash.fromBase58(severalLinks.getJSONObject(j).getString("Hash"));
            jsonRepresentation = ipfsApi.getObjectAsJSON(targetLink);
            data = jsonRepresentation.getString("Data");
            if (data.contains("proto:IPNSTUNNEL")) {
              if (pImmutable) {
                builder.add(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), new IpfsLink("/ipfs/" + ipfsApi.resolveLink(new IpnsLink(data.substring(data.indexOf("(") + 1, data.indexOf(")"))))));
              } else {
                builder.add(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), new IpnsLink(data.substring(data.indexOf("(") + 1, data.indexOf(")"))));
              }
            } else {
              builder.add(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), Link.fromHash(targetLink));
            }
          }
        } else {
          throw new IOException("IPFS prototype expression structure failure. Several Link broken.");
        }
      }
    } else {
      throw new IOException("IPFS prototype expression structure failure. Add Set object broken.");
    }
    
    return builder;
    
  }
  
  /**
   * get IPFS Remove Change Set from the IPFS prototype Expression and add it to the builder.
   * @param builder, which should be extended 
   * @param removeSet, multihash to the remove set
   * @param pImmutable, make an immutable prototype expression.
   * @return extended builder builder
   * @throws IOException
   */
  private PublishedPrototypeExpression.Builder getPrototypeExpressionsRemoveList(PublishedPrototypeExpression.Builder builder, Multihash removeSet, boolean pImmutable) throws IOException{
    JSONArray severalLinks, links;
    String data;
    Multihash targetLink, severalLink;
 
    JSONObject jsonRepresentation = ipfsApi.getObjectAsJSON(removeSet);
    if (jsonRepresentation.getString("Data").equals("proto:REMOVE")) {
      links = jsonRepresentation.getJSONArray("Links");
      for (int i = 0; i < links.length(); i++) {
        severalLink = Multihash.fromBase58(links.getJSONObject(i).getString("Hash"));
        jsonRepresentation = ipfsApi.getObjectAsJSON(severalLink);
        if (jsonRepresentation.getString("Data").equals("proto:SEVERAL")) {
          severalLinks = jsonRepresentation.getJSONArray("Links");
          for (int j = 0; j < severalLinks.length(); j++) {
            targetLink = Multihash.fromBase58(severalLinks.getJSONObject(j).getString("Hash"));
            jsonRepresentation = ipfsApi.getObjectAsJSON(targetLink);
            data = jsonRepresentation.getString("Data");
            if (data.contains("proto:IPNSTUNNEL")) {
              if (pImmutable) {
                builder.remove(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), new IpfsLink("/ipfs/" + ipfsApi.resolveLink(new IpnsLink(data.substring(data.indexOf("(") + 1, data.indexOf(")"))))));
              } else {
                builder.remove(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), new IpnsLink(data.substring(data.indexOf("(") + 1, data.indexOf(")"))));
              }
            } else if (data.contains("proto:ALL")) {
              builder.remove(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), PrototypeExpression.ALL);
            } else {
              builder.remove(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), Link.fromHash(targetLink));
            }
          }
        } else if (jsonRepresentation.getString("Data").equals("proto:ALL")) {
          builder.remove(ID.decodeUtf8(links.getJSONObject(i).getString("Name")), PrototypeExpression.ALL);
        } else {
          throw new IOException("IPFS prototype expression structure failure. Several Link broken.");
        }
      }
    } else {
      throw new IOException("IPFS prototype expression structure failure. Remove Set object broken.");
    }
    
    return builder;
  }

}
