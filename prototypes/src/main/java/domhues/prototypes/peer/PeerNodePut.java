package domhues.prototypes.peer;

import io.ipfs.multihash.Multihash;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Preconditions;

import domhues.prototypes.expressions.ChangeList;
import domhues.prototypes.expressions.IncomingChangeExpression;
import domhues.prototypes.expressions.IncomingPrototypeExpression;
import domhues.prototypes.expressions.PrototypeExpression;
import domhues.prototypes.expressions.PublishedChangeExpression;
import domhues.prototypes.knowledgebase.PrototypeKB;
import domhues.prototypes.links.AllLink;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.IpnsLink;
import domhues.prototypes.links.Link;
import domhues.prototypes.links.PrototypeReference;

/**
 * This class contains all put operations which the peer node offers.
 * 
 * @author dominikhuser
 *
 */
class PeerNodePut {
  PeerNode peer;
  IpfsApi ipfsApi;
  Directory directory;
  
  public PeerNodePut(PeerNode pPeer){
    peer = pPeer;
    ipfsApi = peer.ipfsApi;
    directory = peer.directory;
  }
  
  /**
   * Constructs a IPFS representation for a set of prototype expressions encoded in a file with the following format: 
   * ID: <Prototype Expression ID id> BASE: <Prototype Expression ID base> [-mutable] ADD: <Link name>,<Prototype Expression ID add 1> [-mutable] <...> <Link name>,<Prototype Expression ID add n> [-mutable] REMOVE: <Link name>,<Prototype Expression ID rem 1> [-mutable] <...> <Link name>,<Prototype Expression ID rem m> [-mutable] 
   * Note that the output list does not contain proto:P_0 even if it was defined in the file.
   * All links in this file only can refer to prototype expressions which are defined in the same file as well. 
   * To construct a prototype expression which refers to already published prototype expressions use publishPrototype(PrototypeExpression pProto, boolean pUpdateDir).
   * Pins all recursively used prototype expressions in your node.
   * @param pFile with the mentioned format.
   * @param pUpdateDir defines whether the constructed IPFS representation should be published on IPNS instantly (true), or whether it should be published with further prototype expressions manually, later (false).
   * @return an array of PrototypeReferences. For every constructed IPFS representation this array holds a Prototype Reference. This reference contains the mutable link and the immutable link of this prototype expression.
   */
  public ArrayList<PrototypeReference> prototypeExpression(InputStream pFile, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(pFile);
    ArrayList<PrototypeReference> addedPrototypeExpressions = new ArrayList<PrototypeReference>(); // list of Multihashes which were added (+ maybe published)
    HashMap<String, Multihash> IdHashMapping = new HashMap<String, Multihash>(); // getting hash by id
    HashMap<String, IpnsLink> IdIpnsMapping = new HashMap<String, IpnsLink>(); // getting ipns link by id
    IdHashMapping.put("proto:P_0", PeerNode.PROTO0); // only link which is allowed at the beginning.

    PrototypeExpression.Builder proto;
    PrototypeExpression protoExp;
    Multihash base, add, remove, ipfsLink;
    IpnsLink mutBase, mutAdd, mutRemove, ipnsLink;
    
    // Define correct order to get
    ArrayList<IncomingPrototypeExpression> incProtoList = Parser.getTopologcialOrder(pFile);
    
    // create a new prototype expression from the incoming protototype expression
    for (IncomingPrototypeExpression incProto : incProtoList) {
      proto = new PrototypeExpression.Builder(ID.of(incProto.getIdAsString()));

      // set mtuable or immutable base
      if (incProto.hasMutableBase()) {
        mutBase = IdIpnsMapping.get(incProto.getBase());
        if (mutBase == null) {
          System.out.println(" Base reference " + incProto.getBase() + " is not in IPNS yet.");
          return null;
        }
        proto.setBase(mutBase);
      } else {
        base = IdHashMapping.get(incProto.getBase());
        if (base == null) {
          throw new RuntimeException(" Base reference " + incProto.getBase() + " is not in IPFS yet.");
        }
        proto.setBase(Link.fromHash(base));
      }

      // Add mutable or immutable add links
      for (IncomingChangeExpression incProp : incProto.getAddList()) {
        if (incProp.isMutable()) {
          mutAdd = IdIpnsMapping.get(incProp.getLink());
          if (mutAdd == null) {
            System.out.println("Removed reference is not in IPFS yet.");
            return null;
          }
          proto.add(incProp.getName(), mutAdd);
        } else {
          add = IdHashMapping.get(incProp.getLink());
          if (add == null) {
            System.out.println("Removed reference is not in IPFS yet.");
            return null;
          }
          proto.add(incProp.getName(), Link.fromHash(add));
        }
      }
      
      // Add mutable, delete all or immutable remove links
      for (IncomingChangeExpression incProp : incProto.getRemoveList()) {
        if (incProp.isMutable()) {
          mutRemove = IdIpnsMapping.get(incProp.getLink());
          if (mutRemove == null) {
            System.out.println("Removed reference is not in IPFS yet.");
            return null;
          }
          proto.remove(incProp.getName(), mutRemove);
        } else {

          if (incProp.getLink().equals("proto:ALL")) {
            proto.remove(incProp.getName(), PrototypeExpression.ALL);
          } else {
            remove = IdHashMapping.get(incProp.getLink());
            if (remove == null) {
              System.out.println("Removed reference is not in IPFS yet.");
              return null;
            }
            proto.remove(incProp.getName(), Link.fromHash(remove));
          }
        }
      }
      protoExp = proto.build();
      
      // create the IPFS object but do not publish it via IPNS
      
      //TODO change this back! Only for comparison purpose set to true.
      PrototypeReference ref = prototypeExpression(protoExp, false);
     
      //add prototype reference to return set
      if (!protoExp.getId().equals("proto:P_0")) {
        addedPrototypeExpressions.add(ref);
      }
      
      ipfsLink = ipfsApi.resolveLink(ref.getImmutableLink());
      ipnsLink = ref.getMutableLink();
      
      // Check if there is a double id inside the file. NOT checking whether there is one on the network.
      if (IdHashMapping.containsKey(incProto.getIdAsString()) && !IdHashMapping.get(incProto.getIdAsString()).toBase58().equals(ipfsLink.toBase58())) {
        throw new Error("Same id for different Prototype Expressions found.");
      }
      
      // Add the newly inserted Prototypes Hash and Ipns Link to the two hash tables,
      // such that other prototype expressions can refer immutable or mutable to this prototype expression
      IdHashMapping.put(incProto.getIdAsString(), ipfsLink);
      IdIpnsMapping.put(incProto.getIdAsString(), ipnsLink);
    }
    
    // if wanted publish new dir.
    if (pUpdateDir) {
      directory.publish();
    }   
    return addedPrototypeExpressions;
  }
  
  /**
   * Look at publishPrototype. Accepts file as input.
   * 
   * @param pFile
   * @param pUpdateDir
   * @return List of Prototype References with which one could address the put prototype expressions.
   * @throws IOException
   */
  public ArrayList<PrototypeReference> prototypeExpression(File pFile, boolean pUpdateDir) throws IOException {
    FileInputStream fStream = new FileInputStream(pFile);
    return prototypeExpression(fStream, pUpdateDir);
  }

  
  /**
   * Alternative way to publish a prototype expression via put.  
   * @param pProto
   * @param pUpdateDir
   * @return
   * @throws IOException
   */
  public PrototypeReference prototypeExpression(PrototypeExpression pProto, boolean pUpdateDir) throws IOException {
    ArrayList<byte[]> list;
    JSONObject ipnsTunnel;
    
    // create IPFS add set
    Multihash addHash = createAddSet(pProto);
    
    // create IPFS remove set
    Multihash removeHash = createRemoveSet(pProto);
    
    //Create Prototype Expression JSON Objects remove and add links
    JSONArray prototypeLinks = new JSONArray().
        put(new JSONObject().put("Name", "add").put("Hash", addHash.toBase58())).
        put(new JSONObject().put("Name", "remove").put("Hash", removeHash.toBase58()));
   
    //Add base link to Prototype Expression Object
    if (pProto.getBase() instanceof IpfsLink) {
      //immutable
      prototypeLinks.put(new JSONObject().put("Name", "base").put("Hash", ipfsApi.resolveLink(pProto.getBase()).toBase58()));
    } else {
      //mutable
      ipnsTunnel = new JSONObject().put("Data", "proto:IPNSTUNNEL(" + pProto.getBase().toString() + ")");
      list = new ArrayList<byte[]>();
      list.add(ipnsTunnel.toString().getBytes());
      prototypeLinks.put(new JSONObject().put("Name", "base").put("Hash", ipfsApi.object.put(list).get(0).hash.toBase58()));
    }
    
    //Put Prototype Expression Object on IPFS
    JSONObject prototype = new JSONObject().put("Data", pProto.getId()).put("Links", prototypeLinks);
    list = new ArrayList<byte[]>();
    list.add(prototype.toString().getBytes());
    Multihash result = ipfsApi.object.put(list).get(0).hash;
    
    //Add Protype Expression to Dir
    IpnsLink mutLink = directory.addPrototypeExpression(result);
   
    //Update Dir if wanted
    if (pUpdateDir) {
      directory.publish();
    }
    
    // return both links (immutable an mutable). Remember that the IPNS links is not yet resolvable until the directory is published.
    return new PrototypeReference(Link.fromHash(result), mutLink);
  }
  
  /**
   * Create the Add Set via IPFS put
   * @param pProto, Prototype Expression from which we want to generate the Add set
   * @return Multihash of the Add IPFS object.
   * @throws JSONException
   * @throws IOException
   */
  private Multihash createAddSet(PrototypeExpression pProto) throws JSONException, IOException{   
    //Create Add JSON Object
    Preconditions.checkNotNull(pProto);
    Multihash res;
    JSONObject add = new JSONObject().put("Data", "proto:ADD");
    JSONArray addLink = new JSONArray();
    JSONObject several, ipnsTunnel, target;
    JSONArray severalLink;
    ChangeList addLinks = pProto.getAddList();
    addLinks.sort();
    PublishedChangeExpression current;
    int j;
    int i = 0;
    ArrayList<byte[]> list;
    
    //go through sorted addLinks in O(n).
    while (i < addLinks.getSize()) {
      current = (PublishedChangeExpression) addLinks.getPropertyAt(i);
      several = new JSONObject().put("Data", "proto:SEVERAL");
      severalLink = new JSONArray();
      j = 0;      
      //When properties have the same name then they are next to each other inside the list. 
      while (i + j < addLinks.getSize() && current.getName().equals(addLinks.getPropertyAt(i + j).getName())) {
        Link link = ((PublishedChangeExpression) addLinks.getPropertyAt(i + j)).getLink();
        
        //IPFS Link
        if (link instanceof IpfsLink) {
          target = new JSONObject().put("Name", Integer.toString(j)).put("Hash", ipfsApi.resolveLink(((PublishedChangeExpression) addLinks.getPropertyAt(i + j)).getLink()));
          severalLink.put(target);
          
        //IPNS Link
        } else if (link instanceof IpnsLink) {
          ipnsTunnel = new JSONObject().put("Data", "proto:IPNSTUNNEL(" + link.toString() + ")").put("Links", new JSONArray());
          list = new ArrayList<byte[]>();
          list.add(ipnsTunnel.toString().getBytes());
          severalLink.put(new JSONObject().put("Name", Integer.toString(j)).put("Hash", ipfsApi.object.put(list).get(0).hash.toBase58()));
          
        //All Link in Add Set (failure)
        } else if (link instanceof AllLink) {
          throw new Error("All-links are not allowed in the Add set.");
          
        //Other Failure  
        } else {
          throw new Error("Unknown Link Type failure.");
        }
        j++;
      }
      i += j;
      
      // Put several object for same named properties on IPS
      several.put("Links", severalLink);
      list = new ArrayList<byte[]>();
      list.add(several.toString().getBytes());
      res = ipfsApi.object.put(list).get(0).hash;
      
      // Add "several" object to IPFS
      addLink.put(new JSONObject().put("Name", ID.encodeUtf8(current.getName())).put("Hash", res.toBase58()));
    }

    // Finish add JSON object and put on IPFS
    add.put("Links", addLink);
    list = new ArrayList<byte[]>();
    list.add(add.toString().getBytes());
    return ipfsApi.object.put(list).get(0).hash;
    
    
  }
  
  /**
   * Create the Remove Set via IPFS put
   * Analog to createAddSet
   * @param pProto, Prototype Expression from which we want to generate the Remove set
   * @return Multihash of the Add IPFS object.
   * @throws JSONException
   * @throws IOException   
   */
  private Multihash createRemoveSet(PrototypeExpression pProto) throws JSONException, IOException{  
    Preconditions.checkNotNull(pProto);
    Multihash res;
    JSONObject several, ipnsTunnel, target;
    JSONArray severalLink;
    PublishedChangeExpression current;
    ArrayList<byte[]> list;  
    JSONObject remove = new JSONObject().put("Data", "proto:REMOVE");
    JSONArray remLink = new JSONArray();
    ChangeList remLinks = pProto.getRemoveList();
    remLinks.sort();
    int j = 0;
    int i = 0;
    
    while (i < remLinks.getSize()) {
      current = (PublishedChangeExpression) remLinks.getPropertyAt(i);
      if (current.getLink() instanceof AllLink) {
        res = PeerNode.ALL;
        i++;
      } else {
        several = new JSONObject().put("Data", "proto:SEVERAL");
        severalLink = new JSONArray();
        j = 0;
        while (i + j < remLinks.getSize() && current.getName().equals(remLinks.getPropertyAt(i + j).getName())) {
          Link link = ((PublishedChangeExpression) remLinks.getPropertyAt(i + j)).getLink();
          if (link instanceof IpfsLink) {
            target = new JSONObject().put("Name", Integer.toString(j)).put("Hash", ipfsApi.resolveLink(((PublishedChangeExpression) remLinks.getPropertyAt(i + j)).getLink()));
            severalLink.put(target);
          } else {

            ipnsTunnel = new JSONObject().put("Data", "proto:IPNSTUNNEL(" + link.toString() + ")").put("Links", new JSONArray());
            list = new ArrayList<byte[]>();
            list.add(ipnsTunnel.toString().getBytes());
            severalLink.put(new JSONObject().put("Name", Integer.toString(j)).put("Hash", ipfsApi.object.put(list).get(0).hash.toBase58()));
          }
          j++;
        }
        i += j;
        several.put("Links", severalLink);
        list = new ArrayList<byte[]>();
        list.add(several.toString().getBytes());
        res = ipfsApi.object.put(list).get(0).hash;
      }
      remLink.put(new JSONObject().put("Name", ID.encodeUtf8(current.getName())).put("Hash", res.toBase58()));
    }
    
    //Put remove JSON Object on IPFS
    remove.put("Links", remLink);
    list = new ArrayList<byte[]>();
    list.add(remove.toString().getBytes());
    return ipfsApi.object.put(list).get(0).hash;

    
  }
  
  /**
   * Publishes a knowledge base on your own node. Use self constructed prototype expressions or protype expressions from other nodes. Prototype expressions from other nodes will be pinned to your own node as long as it is part of your knowledge base
   * 
   * @param pKB a PrototypeKB object.
   * @param pUpdateDir defines whether the constructed IPFS representation should be published on IPNS instantly (true),
   * @return the hash of the Ipfs representation.
   * @throws IOException
   */
  public Multihash knowledgeBase(PrototypeKB pKB, boolean pUpdateDir) throws IOException {
    Preconditions.checkNotNull(pKB);
    Preconditions.checkNotNull(pUpdateDir);
  
    // Define a new IPFS object representing a KB.
    Multihash kbHash = createKBObject(pKB);

    directory.addKnowledgeBase(kbHash, pKB.getName());
    //update dir if wanted
    if (pUpdateDir) {
      directory.publish();
    }
    return kbHash;
  }
  
  
  /**
   * Create the knowledge base object.
   * @param pKB
   * @return
   * @throws IOException
   */
  private Multihash createKBObject(PrototypeKB pKB) throws IOException{  
    ArrayList<Link> protoList = pKB.getPrototypeExpressions();
    JSONObject kbObject = new JSONObject().put("Data", pKB.getName().toString());
    JSONArray links = new JSONArray();
    int count = 0;
    for (Link link : protoList) {
      if (link instanceof IpfsLink) {
        Multihash hashLink = ipfsApi.resolveLink(link);
        links.put(new JSONObject().put("Name",Integer.toString(count)).put("Hash",hashLink));
      } else if (link instanceof IpnsLink) {
        // Generate a forwarding tunnel object.
        Multihash ipnsTunnel = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:IPNSTUNNEL(" + link.toString() + ")");
        links.put(new JSONObject().put("Name",Integer.toString(count)+ "-mutable").put("Hash",ipnsTunnel));
      }
      count++;
    } 
    kbObject.put("Links", links);
    
    // put on IPFS.
    ArrayList<byte[]> list = new ArrayList<byte[]>();
    list.add(kbObject.toString().getBytes());
    return ipfsApi.object.put(list).get(0).hash;
 }
  
  
  /**
   * @deprecated
   * Constructs an IPFS representation of a Prototype Expression via IPFS PATCH.
   * @param pProto
   * @return A PrototypeReference. This reference contains the mutable link and the immutable link of this prototype expression.
   * @throws IOException
   */
  public PrototypeReference prototypeExpressionDepricated(PrototypeExpression pProto, boolean pUpdateDir) throws IOException {
    
    Preconditions.checkNotNull(pProto);
    
    // create new add change set
    Multihash add = createAddObject(pProto);
    
    // create remove set    
    Multihash remove = createRemoveObject(pProto);

    // generate prototype IPFS object.
    Multihash prot = createPrototypeExpressionObject(pProto, add, remove);
    
    // Add new prototype expression to dir.
    IpnsLink mutLink = directory.addPrototypeExpression(prot);
    
    //republish dir if wanted
    if (pUpdateDir) {
      directory.publish();
    }    
    return new PrototypeReference((IpfsLink) Link.fromHash(prot), mutLink);
  }
  
  /**
   * @deprecated
   * create the remove IPFS part of an prototype expression
   * @param pProto, prototype expression 
   * @return the multihash of the remove part of the IPFS representation
   * @throws IOException
   */
  private Multihash createRemoveObject(PrototypeExpression pProto) throws IOException{
    PublishedChangeExpression current;
    Multihash SEVERAL = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:SEVERAL");
    Multihash several, ipnsTunnel;
    int j;
    int i = 0;
    Multihash remove = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:REMOVE");
    Multihash removeAll = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:ALL");
    ChangeList remLinks = pProto.getRemoveList();
    remLinks.sort();

    // add every remove change expression to the remove set
    while (i < remLinks.getSize()) {
      current = (PublishedChangeExpression) remLinks.getPropertyAt(i);
      if (current.getLink() instanceof AllLink) {
        remove = ipfsApi.patchAddLink(remove, current.getName().toString(), removeAll);
        i++;
      } else {
        several = SEVERAL;
        j = 0;
        while (i + j < remLinks.getSize() && current.getName().equals(remLinks.getPropertyAt(i + j).getName())) {
          Link link = ((PublishedChangeExpression) remLinks.getPropertyAt(i + j)).getLink();
          if (link instanceof IpfsLink) {
            several = ipfsApi.patchAddLink(several, Integer.toString(j), ipfsApi.resolveLink(((PublishedChangeExpression) remLinks.getPropertyAt(i + j)).getLink()));
          } else if (link instanceof IpnsLink) {
            ipnsTunnel = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:IPNSTUNNEL(" + link.toString() + ")");
            several = ipfsApi.patchAddLink(several, Integer.toString(j) + "-mutable", ipnsTunnel);

          } else {
            throw new Error("Unknown Link Type failure.");
          }
          j++;
        }
        i += j;
        remove = ipfsApi.patchAddLink(remove, ID.encodeUtf8(current.getName()), several);
        several = SEVERAL;
      }
    }   
    return remove;
  }
  
  /**
   * @deprecated
   * create the add IPFS part of an prototype expression
   * @param pProto
   * @return
   * @throws IOException
   */
  private Multihash createAddObject(PrototypeExpression pProto) throws IOException{
    Multihash add = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:ADD");
    ChangeList addLinks = pProto.getAddList();
    //sort change expressions such that several expressions with the same id get the same several object.
    addLinks.sort();
    PublishedChangeExpression current;
    Multihash SEVERAL = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:SEVERAL");
    Multihash several, ipnsTunnel;
    int j;
    int i = 0;
    
    // add every add change expression to the add object.
    while (i < addLinks.getSize()) {
      current = (PublishedChangeExpression) addLinks.getPropertyAt(i);
      several = SEVERAL;
      j = 0;
      while (i + j < addLinks.getSize() && current.getName().equals(addLinks.getPropertyAt(i + j).getName())) {
        Link link = ((PublishedChangeExpression) addLinks.getPropertyAt(i + j)).getLink();
        if (link instanceof IpfsLink) {
          several = ipfsApi.patchAddLink(several, Integer.toString(j), ipfsApi.resolveLink(((PublishedChangeExpression) addLinks.getPropertyAt(i + j)).getLink()));
        } else if (link instanceof IpnsLink) {
          ipnsTunnel = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:IPNSTUNNEL(" + link.toString() + ")");
          several = ipfsApi.patchAddLink(several, Integer.toString(j) + "-mutable", ipnsTunnel);
        } else if (link instanceof AllLink) {
          throw new Error("All-links are not allowed in the Add set.");
        } else {
          throw new Error("Unknown Link Type failure.");
        }
        j++;
      }
      i += j;
      add = ipfsApi.patchAddLink(add, ID.encodeUtf8(current.getName()), several);
      several = SEVERAL;
    }
    return add;
  }

  /**
   * @deprecated
   * Create the IPFS prototype expression obejct which holds links to add and remove objects.
   * @param pProto
   * @param add
   * @param remove
   * @return
   * @throws IOException
   */
  private Multihash createPrototypeExpressionObject(PrototypeExpression pProto, Multihash add, Multihash remove) throws IOException{
    Multihash ipnsTunnel;
    Multihash prot = PeerNode.EMPTY;
    prot = ipfsApi.patchSetData(prot, pProto.getId().toString());
    prot = ipfsApi.patchAddLink(prot, "add", add);
    prot = ipfsApi.patchAddLink(prot, "remove", remove);
    if (!pProto.getId().equals("proto:P_0")) {
      Link baseLink = pProto.getBase();
      if (baseLink instanceof IpfsLink) {
        prot = ipfsApi.patchAddLink(prot, "base", ipfsApi.resolveLink(baseLink));
      } else {
        ipnsTunnel = ipfsApi.patchSetData(PeerNode.EMPTY, "proto:IPNSTUNNEL(" + baseLink.toString() + ")");
        prot = ipfsApi.patchAddLink(prot, "base", ipnsTunnel);
      }
    }
    return prot;
  }

}


