package domhues.prototypes.links;

import java.io.IOException;

import com.google.common.base.Preconditions;

import io.ipfs.multihash.Multihash;

/**
 * Abstract Link class. Containing a Link, as well as useful static functions.
 * 
 * @author dominikhuser
 *
 */
public abstract class Link {

  String link;

  /**
   * Constructor 
   * @param pLink of the form /<ipfs or ipns>/<hash>/.../...
   */
  public Link(String pLink) {
    link = pLink;
  }

  /**
   * Link to string.
   */
  public String toString() {
    return link;
  }

  /**
   * deletes s1 from a String pText "/s1/s2/../sn"
   * 
   * @param pText
   * @return /s2/../sn
   */
  public static String deleteFirstSegment(String pText) throws IOException {
    Preconditions.checkNotNull(pText);
    if (!pText.contains("/") || (pText.indexOf("/") != 0)) {
      throw new IOException("Wrong link format: " + pText);
    } else if (pText.indexOf("/", 1) != -1) {
      return pText.substring(pText.indexOf("/", 1), pText.length());
    } else {
      return "";
    }
  }
  
  /**
   * Checks whether the link is not too long. Useful to check if a link refers to an IPFS object via Segments-1 other object. 
   * Remember that the prefix /ipfs and /ipns are segements of a link as well. 
   * @param link
   * @param maxSegments
   * @throws IOException
   */
  public static void checkLinkLength(Link pLink, int segments) throws IOException {
    Preconditions.checkNotNull(pLink);
    Preconditions.checkNotNull(segments);
    String link = pLink.toString();
    int count = link.length() - link.replace("/", "").length();
    if(count==segments){
      return;
    }else{
      throw new IOException("Link is too long (expectet " + segments+" segments):" + link);
    }
  }

  /**
   * Gets s1 of a String pText "/s1/s2/../sn"
   * 
   * @param pText
   * @return the first segment (s1).
   */
  public static String getFirstSegment(String pText) throws IOException {
    Preconditions.checkNotNull(pText);
    Preconditions.checkNotNull(pText);
    if (!pText.contains("/") || (pText.indexOf("/") != 0)) {
      throw new IOException("Wrong link format:" + pText);
    } else if (pText.indexOf("/", 1) != -1) {
      return pText.substring(1, pText.indexOf("/", 1));
    } else {
      return pText.substring(1, pText.length());
    }
  }

  /**
   * Returns an IpfsLink defined by a Hash.
   * 
   * @param pHash Multihash 
   * @return IpfsLink.
   */
  public static IpfsLink fromHash(Multihash pHash) {
    Preconditions.checkNotNull(pHash);
    return new IpfsLink("/ipfs/" + pHash.toBase58());

  }
}
