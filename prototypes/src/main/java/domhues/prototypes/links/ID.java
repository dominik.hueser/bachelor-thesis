package domhues.prototypes.links;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.abdera.i18n.iri.IRI;
import org.apache.abdera.i18n.iri.IRISyntaxException;

import com.google.common.base.Preconditions;

/**
 * An ID, which can be any absolute IRI
 * 
 * An ID is immutable
 * 
 * @author michael cochez (Prototype). Slightly adapted due to two encoding/decoding methods.
 *
 */
public class ID {
    private final String value;

    /**
     * Creates an ID from the given value
     * 
     * @param value
     *            the IRI
     * @return an ID
     * @throws IRISyntaxException
     *             if the string is not a valid IRI
     * @throws IOException 
     * @throws IllegalArgumentException
     *             if the iri is not absolute
     */
    public static ID of(String value) throws IRISyntaxException, IOException {
        // creating a URI type is about three times faster as a IRI type, so we
        // try that first
        // This might be implementation dependent.

        String uriString;
        try {
            URI uri = new URI(value);
            if(!uri.isAbsolute()){
              throw new IOException("URI mus be absolute!");
            }
            uriString = uri.toString();
        } catch (URISyntaxException e) {
            IRI iri = new IRI(value);
            if(!iri.isAbsolute()){
              throw new IOException("IRI mus be absolute!");
            }
            uriString = iri.toString();
        }
        return new ID(uriString);
    }

    /**
     * Constructs an ID from the IRI
     * 
     * @param iri
     * @throws NullPointerException
     *             if iri is null
     * @throws IllegalArgumentException
     *             if the iri is not absolute
     */
    public ID(IRI iri) {
        Preconditions.checkNotNull(iri);
        Preconditions.checkArgument(iri.isAbsolute(), "Only absolute IRIs are valid IDs");
        this.value = iri.toString();
    }

    /**
     * Constructs an ID from the URI
     * 
     * @param uri
     * @throws NullPointerException
     *             if uri is null
     * @throws IllegalArgumentException
     *             if the uri is not absolute
     */
    public ID(URI uri) {
        Preconditions.checkNotNull(uri);
        Preconditions.checkArgument(uri.isAbsolute(), "Only absolute IRIs are valid IDs");
        this.value = uri.toString();
    }
    
    /**
     * Encode IRI to a URL encoding: work around special characters according to https://github.com/ipfs/go-ipfs/pull/1740
     * @author Dominik Hüser
     * @param pId
     * @return
     * @throws UnsupportedEncodingException 
     */
    public static String encodeUtf8(ID pId) throws UnsupportedEncodingException {
      return URLEncoder.encode(pId.toString(), "UTF-8");
    }
    
    /**
     * Decode IRI from URL encoding: work around special characters according to https://github.com/ipfs/go-ipfs/pull/1740 and the encoding defined in encodeUtf8(ID pId).
     * @author Dominik Hüser
     * @param pId
     * @return
     * @throws IRISyntaxException
     * @throws IOException
     */
    public static ID decodeUtf8(String pId) throws IRISyntaxException, IOException{
      return ID.of(URLDecoder.decode(pId, "UTF-8"));
    }
    

    /**
     * Constructs an ID from a String. This is private because the String value
     * is not checked.
     * 
     * @param uri
     * @throws NullPointerException
     *             if uri is null
     * @throws IllegalArgumentException
     *             if the uri is not absolute
     */
    private ID(String uriString) {
        Preconditions.checkNotNull(uriString);
        this.value = uriString;
    }

    /**
     * get the IRI from the ID
     * 
     * @return
     */
    public IRI getValue() {
        return new IRI(this.value);
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ID other = (ID) obj;
        return this.value.equals(other.value);
    }

    /**
     * The string representation of the ID, this will be the string
     * representation of the IRI.
     */
    @Override
    public String toString() {
        return this.value;
    }

}
