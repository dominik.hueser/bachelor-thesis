package domhues.prototypes.links;


/**
 * Representation of an IPFS Link /ipfs/<ipfs-hash>/<ipfs-path>
 * 
 * @author dominikhuser
 *
 */
public class IpfsLink extends Link {
  /**
   * Constructor.
   * 
   * @param pLink off the format /ipfs/<ipfs-hash>/<ipfs-path>
   */
  public IpfsLink(String pLink) {
    super(pLink);
  }

}
