package domhues.prototypes.links;


/**
 * Representation of an IPNS link: /ipns/<peer-node>/<path>
 * 
 * @author dominikhuser
 *
 */
public class IpnsLink extends Link {
  /**
   * Constructor.
   * 
   * @param pLink off the format /ipns/<peer-node>/<path>
   */
  public IpnsLink(String pLink) {
    super(pLink);
  }
}
