package domhues.prototypes.links;


/**
 * A reference contains two ways of reaching a prototype expression. A mutable and an immutable link.
 * 
 * @author dominikhuser
 *
 */
public class PrototypeReference {
  private IpfsLink immLink;
  private IpnsLink mutLink;

  /**
   * Construct a PrototypeReference.
   * 
   * @param pImmLink
   * @param pMutLink
   */
  public PrototypeReference(IpfsLink pImmLink, IpnsLink pMutLink) {
    immLink = pImmLink;
    mutLink = pMutLink;
  }

  /**
   * Return the immutable link.
   * 
   * @return immutable link (IpfsLink).
   */
  public IpfsLink getImmutableLink() {
    return immLink;
  }

  /**
   * Return the mutable link.
   * 
   * @return mutable link (IpnsLink)
   */
  public IpnsLink getMutableLink() {
    return mutLink;
  }


}
