package domhues.prototypes.links;


/**
 * Representation of the All Link. For example in an remove set of a PE. 
 * @author dominikhuser
 *
 */
public class AllLink extends Link {

  public AllLink() {
    super("");
  }
}
