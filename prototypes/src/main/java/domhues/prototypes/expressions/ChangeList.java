package domhues.prototypes.expressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.common.base.Preconditions;

import domhues.prototypes.links.ID;
import domhues.prototypes.links.Link;

/**
 * A list of Change Expressions which can be used to represent the add or remove set of a prototype expression.
 * 
 * @author dominikhuser
 *
 */
public class ChangeList {
  ArrayList<PublishedChangeExpression> list;

  /**
   * Constructor initializes an empty change list.
   */
  public ChangeList() {
    list = new ArrayList<PublishedChangeExpression>();
  }

  /**
   * Add a Change Expression
   * 
   * @param pName
   * @param pLink
   */
  public void add(ID pName, Link pLink) {
    Preconditions.checkNotNull(pName);
    Preconditions.checkNotNull(pLink);
    PublishedChangeExpression prop = new PublishedChangeExpression(pName, pLink);
    list.add(prop);
  }

  /**
   * Get a property by it's position
   * 
   * @param pPos
   * @return
   */
  public Property getPropertyAt(int pPos) {
    return list.get(pPos);
  }

  /**
   * Get size of the change set
   * 
   * @return
   */
  public int getSize() {
    return list.size();
  }

  /**
   * sort by property (name)
   */
  public void sort() {
    Collections.sort(list, new Comparator<Property>() {
      public int compare(Property p1, Property p2) {
        return p1.getName().toString().compareTo(p2.getName().toString());
      }
    });

  }
}
