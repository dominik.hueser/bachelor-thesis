package domhues.prototypes.expressions;

import io.ipfs.multihash.Multihash;

import java.io.File;
import java.util.HashSet;
import java.util.Objects;

import com.google.common.base.Preconditions;

import domhues.prototypes.links.ID;

/**
 * A Protoype is the result of a fixpoint computation. Different to prototype expressions prototypes contain a list of all properties directly.
 * 
 * @author dominikhuser
 *
 */
public class Prototype {

  private ID id;
  private Multihash hash;
  HashSet<PrototypeProperty> properties;

  /**
   * Construct a Prototype Expression with this pId
   * 
   * @param pId
   */
  public Prototype(ID pId, Multihash pHash) {
    
    id = pId;
    hash = pHash;
    // TODO find good initial capacity
    properties = new HashSet<PrototypeProperty>(8);
  }

  /**
   * Get Id
   * 
   * @return
   */
  public ID getId() {
    return id;
  }

  /**
   * Add a new property containing a ID and a link
   * 
   * @param pValue
   * @param pName
   */
  public void add(ID pName, Multihash pValue) {
    Preconditions.checkNotNull(pName);
    Preconditions.checkNotNull(pValue);
    PrototypeProperty prop = new PrototypeProperty(pName, pValue);
    properties.add(prop);
  }

  /**
   * Remove a property of a Prototype
   * 
   * @param pValue
   * @param pName
   */
  public void remove(ID pName, Multihash pValue) {
    Preconditions.checkNotNull(pName);
    Preconditions.checkNotNull(pValue);
    PrototypeProperty prop = new PrototypeProperty(pName, pValue);
    properties.remove(prop);
  }

  /**
   * Remove all function. Removes every PrototypeProperty which has the name pName
   * 
   * @param pName
   */
  public void remove(ID pName) {
    HashSet<PrototypeProperty> removeCandidate = new HashSet<PrototypeProperty>();
    Preconditions.checkNotNull(pName);
    for (PrototypeProperty prop : properties) {
      if (prop.getName().toString().equals(pName.toString())) {
        removeCandidate.add(prop);
      }
    }
    properties.removeAll(removeCandidate);
  }


  public HashSet<PrototypeProperty> getProperties() {
    return properties;
  }

  /**
   * print a Prototype
   */
  public void print() {
    System.out.println("HASH-ID:");
    System.out.println(hash);
    System.out.println("DESCRIPTION:");
    System.out.println(id);
    System.out.println("PROPERTIES:");
    for (PrototypeProperty prop : properties) {
      System.out.println(prop.getName() + " " + prop.getValue());
    }
    System.out.println("");
  }

  /**
   * print a Prototype to file.
   * 
   * @param pFile
   */
  public void print(File pFile) {
    //TODO code
  }

  /**
   * A property of a Prototype.
   * 
   * @author dominikhuser
   *
   */
  public class PrototypeProperty extends Property {

    private Multihash value;

    /**
     * Constructor
     * @param pName
     * @param pValue
     */
    public PrototypeProperty(ID pName, Multihash pValue) {
      super(pName);
      value = pValue;
    }

    /**
     * get value
     * 
     * @return
     */
    public Multihash getValue() {
      return value;
    }


    @Override
    public int hashCode() {
      return Objects.hash(super.name, value);
    }

    @Override
    public boolean equals(Object pObject) {
      PrototypeProperty prop = (PrototypeProperty) pObject;
      return Objects.equals(prop.getName(), this.name) && Objects.equals(prop.getValue(), this.value);
    }

  }

}
