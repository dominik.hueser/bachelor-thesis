package domhues.prototypes.expressions;

import com.google.common.base.Preconditions;

import io.ipfs.multihash.Multihash;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.Link;


/**
 * A published prototype expression is a prototype expression which is already published on the IPFS network. 
 * Therefore, it as an additional attribute: A hash.
 * 
 * @author dominikhuser
 *
 */
public class PublishedPrototypeExpression extends PrototypeExpression {
  Multihash hash;

  /**
   * Constructor, called by Builder Class
   * @param pId
   * @param pBase
   * @param pAddSet
   * @param pRemoveSet
   * @param pHash
   */
  PublishedPrototypeExpression(ID pId, Link pBase, ChangeList pAddSet, ChangeList pRemoveSet, Multihash pHash) {
    super(pId, pBase, pAddSet, pRemoveSet);
    hash = pHash;
  }
  
  
  /**
   * Get the prototype expression's hash as IpfsLink
   * @return
   */
  public IpfsLink getIpfsLink() {
    return new IpfsLink("/ipfs/" + hash.toBase58());
  }

  /**
   * Get the prototype expression's hash.
   * @return
   */
  public Multihash getHash() {
    return hash;
  }


  /**
   * Builder for a Published Prototype Expression. Extends the builder of a Prototype Expression
   * 
   * @author dominikhuser
   *
   */
  public static class Builder {
    Multihash hash;
    protected final ID id;
    protected ChangeList addList;
    protected ChangeList removeList;
    protected Link base;

    /**
     * add the prototype id.
     * 
     * @param pId
     */
    public Builder(ID pId) {
      Preconditions.checkNotNull(pId);
      id = pId;
      addList = new ChangeList();
      removeList = new ChangeList();
      // TODO insert define for has of Proto P0
      base = new IpfsLink("");
    }

    /**
     * Set the Base link
     * 
     * @param pLink
     * @return Builder
     */
    public Builder setBase(Link pLink) {
      Preconditions.checkNotNull(pLink);
      base = pLink;
      return this;
    }

    /**
     * Add a published prototype expression to the add set.
     * 
     * @param pName
     * @param pProto
     * @return Builder
     */
    public Builder add(ID pName, PublishedPrototypeExpression pProto) {
      Preconditions.checkNotNull(pProto);
      Preconditions.checkNotNull(pName);
      return this.add(pName, pProto.getIpfsLink());
    }

    /**
     * Add a published prototype expression to the add set by giving its name
     * 
     * @param pName
     * @param pLink
     * @return Builder
     */
    public Builder add(ID pName, Link pLink) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pLink);
      addList.add(pName, pLink);
      return this;

    }

    /**
     * Add a published prototype expression to the remove set.
     * 
     * @param pName
     * @param pProto
     * @return Builder
     */
    public Builder remove(ID pName, PublishedPrototypeExpression pProto) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pProto);
      return this.remove(pName, pProto.getIpfsLink());
    }

    /**
     * Add a published prototype expression to the remove set by giving its name
     * 
     * @param pName
     * @param pLink
     * @return Builder
     */
    public Builder remove(ID pName, Link pLink) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pLink);
      removeList.add(pName, pLink);
      return this;
    }

    /**
     * Add IpfsLink
     * 
     * @param pLink
     * @return Builder
     */
    public Builder addHash(Multihash pHash) {
      Preconditions.checkNotNull(pHash);
      hash = pHash;
      return this;
    }

    /**
     * Build
     * @return PublishedPrototypeExpression
     */
    public PublishedPrototypeExpression build() {
      return new PublishedPrototypeExpression(id, base, addList, removeList, hash);
    }
  }



}
