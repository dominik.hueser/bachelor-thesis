package domhues.prototypes.expressions;

import domhues.prototypes.links.ID;
import domhues.prototypes.links.Link;

/**
 * A published change expression is a change expression having a Link as target and not, like an incoming CE, an ID.
 * 
 * @author dominikhuser
 *
 */
public class PublishedChangeExpression extends Property {

  Link target;
  
/**
 * Constructor
 * @param pName
 * @param pTarget
 */
  public PublishedChangeExpression(ID pName, Link pTarget) {
    super(pName);
    target = pTarget;
  }

  /**
   * Get Link
   * @return
   */
  public Link getLink() {
    return target;
  }
}
