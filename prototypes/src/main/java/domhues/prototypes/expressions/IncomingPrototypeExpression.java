package domhues.prototypes.expressions;

import java.util.ArrayList;

import com.google.common.base.Preconditions;

import domhues.prototypes.links.ID;

/**
 * An incoming prototype expression.
 * 
 * @author dominikhuser
 *
 */
public class IncomingPrototypeExpression {

  private ID id;
  private IncomingChangeExpression base;
  private ArrayList<IncomingChangeExpression> addList;
  private ArrayList<IncomingChangeExpression> removeList;

  /**
   * Constructor, only called by builder.
   * @param pId, ID
   * @param pBase, PE base
   * @param pAddList, PE AddSet as List of IncomingChangeExpression
   * @param pRemoveList, PE RemoveSet as List of IncomingChangeExpression
   */
  IncomingPrototypeExpression(ID pId, IncomingChangeExpression pBase, ArrayList<IncomingChangeExpression> pAddList, ArrayList<IncomingChangeExpression> pRemoveList) {
    id = pId;
    base = pBase;
    addList = pAddList;
    removeList = pRemoveList;
  }

  /**
   * Get ID
   * @return ID as String
   */
  public String getIdAsString() {
    return id.toString();
  }

  /**
   * Get IncomingChangeExpression Add List
   * @return ArrayList of IncomingChangeExpression
   */
  public ArrayList<IncomingChangeExpression> getAddList() {
    return addList;
  }

  /**
   * Get IncomingChangeExpression Remove List
   * @return ArrayList of IncomingChangeExpression
   */
  public ArrayList<IncomingChangeExpression> getRemoveList() {
    return removeList;
  }

/**
 * Get Base Link as String
 * @return String 
 */
  public String getBase() {
    return base.getLink();
  }

  /**
   * Return true if mutable base else false
   * @return boolean
   */
  public boolean hasMutableBase() {
    return base.isMutable();
  }

  /**
   * Builder
   * 
   * @author dominikhuser
   *
   */
  public static class Builder {

    private ID id;
    private IncomingChangeExpression base;
    private ArrayList<IncomingChangeExpression> addList;
    private ArrayList<IncomingChangeExpression> removeList;

    /**
     * set Id and base
     * 
     * @param pId
     * @param pBase
     */
    public Builder(ID pId, IncomingChangeExpression pBase) {
      id = pId;
      base = pBase;
      addList = new ArrayList<IncomingChangeExpression>();
      removeList = new ArrayList<IncomingChangeExpression>();
    }

    /**
     * add a new property to the add list
     * 
     * @param pName
     * @param pTarget
     * @param pMutable
     * @return
     */
    public Builder add(ID pName, ID pTarget, boolean pMutable) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pTarget);
      addList.add(new IncomingChangeExpression(pName, pTarget, pMutable));
      return this;
    }

    /**
     * add a new property to the remove list
     * 
     * @param pName
     * @param pTarget
     * @param pMutable
     * @return
     */
    public Builder remove(ID pName, ID pTarget, boolean pMutable) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pTarget);
      removeList.add(new IncomingChangeExpression(pName, pTarget, pMutable));
      return this;
    }

    /**
     * build
     * 
     * @return
     */
    public IncomingPrototypeExpression build() {
      return new IncomingPrototypeExpression(id, base, addList, removeList);
    }


  }
}
