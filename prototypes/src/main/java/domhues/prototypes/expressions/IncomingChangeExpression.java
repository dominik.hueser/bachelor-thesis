package domhues.prototypes.expressions;

import domhues.prototypes.links.ID;

/**
 * A incoming change expression contains a target ID and a name of th etype ID.
 * 
 * @author dominikhuser
 *
 */
public class IncomingChangeExpression extends Property {
  ID target;
  boolean mutable;

  /**
   * Constructor
   * 
   * @param pName
   * @param pTarget
   * @param pMutable links can be mutable by setting this flag.
   */
  public IncomingChangeExpression(ID pName, ID pTarget, boolean pMutable) {
    super(pName);
    target = pTarget;
    mutable = pMutable;
  }
  
  /**
   * Return true if it is a mutable change expression. Else false.
   * @return
   */
  public boolean isMutable() {
    return mutable;
  }
  
  /**
   * Get Link as String 
   * @return
   */
  public String getLink() {
    return target.toString();
  }
  
  /**
   * Get Link as ID
   * @return
   */
  public ID getLinkAsID() {
    return target;
  }

}
