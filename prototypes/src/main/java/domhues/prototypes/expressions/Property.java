package domhues.prototypes.expressions;

import domhues.prototypes.links.ID;

/**
 * Abstract Property contains an ID.
 * 
 * @author dominikhuser
 *
 */
public abstract class Property {

  protected ID name;

  /**
   * Constructor
   * 
   * @param pName
   */
  public Property(ID pName) {
    name = pName;
  }

  /**
   * get Name
   * 
   * @return
   */
  public ID getName() {
    return name;
  }


}
