package domhues.prototypes.expressions;

import java.io.IOException;

import com.google.common.base.Preconditions;

import domhues.prototypes.links.AllLink;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.IpfsLink;
import domhues.prototypes.links.Link;
import domhues.prototypes.peer.PeerNode;


/**
 * A PortotypeExpression can be used to derive new PrototypeExpressions from already published prototype expressions.
 * Create a PrototypeExpression via a Builder.
 * Default base is proto:P_0.
 * 
 * @author dominikhuser
 *
 */


public class PrototypeExpression {

  // User uses these to refer to All and proto P0
  public static IpfsLink ZERO = Link.fromHash(PeerNode.PROTO0);

  public static AllLink ALL = new AllLink();

  private final ID id;
  private ChangeList addList;
  private ChangeList removeList;
  private Link base;

  /**
   * Constructor of a Prototype, only called by Builder class
   * @param pId, PE ID
   * @param pBase Base
   * @param pAddList Add List ChangeList
   * @param pRemoveList Remove List ChangeList
   */
  PrototypeExpression(ID pId, Link pBase, ChangeList pAddList, ChangeList pRemoveList) {
    id = pId;
    addList = pAddList;
    removeList = pRemoveList;
    base = pBase;
  }

  /**
   * The IpfsLink to proto:P_0
   * @return IpfsLink of P_0
   */
  public static IpfsLink Zero() {
    return Link.fromHash(PeerNode.PROTO0);
  }

  /**
   * Get Id
   * 
   * @return ID
   */
  public ID getId() {
    return id;
  }

  /**
   * Get Add-Change Set
   * 
   * @return ChangeList
   */
  public ChangeList getAddList() {
    return addList;
  }

  /**
   * Get Remove-Change Set
   * 
   * @return ChangeList
   */
  public ChangeList getRemoveList() {
    return removeList;
  }

  /**
   * Get base
   * 
   * @return Link
   */
  public Link getBase() {
    return base;
  }
  
  /**
   * Alternative way to put a constructed prototype expression on ProtoIPFS. 
   * Does not publish the Dir. 
   * @param peer
   * @throws IOException
   */
  public void putOn(PeerNode peer, boolean publishDir) throws IOException{
    peer.putPrototypeExpression(this, publishDir);
  }
  
  /**
   * Print Prototype Expression
   */
  public void print() {
    PublishedChangeExpression prop;
    System.out.println("ID: " + this.getId());
    System.out.println("BASE: " + this.getBase());
    for (int i = 0; i<addList.getSize(); i++) {
      prop = (PublishedChangeExpression) addList.getPropertyAt(i);
      System.out.println("ADD: " + prop.getName() + " " + prop.getLink());
    }
    for (int i = 0; i<removeList.getSize(); i++) {
      prop = (PublishedChangeExpression) removeList.getPropertyAt(i);
      System.out.println("REMOVE: " + prop.getName() + " " + prop.getLink());
    }
  }

  /**
   * Builder class
   * 
   * @author dominikhuser
   *
   */
  public static class Builder {
    protected final ID id;
    protected ChangeList addList;
    protected ChangeList removeList;
    protected Link base;

    /**
     * add the prototype id.
     * 
     * @param pId
     */
    public Builder(ID pId) {
      id = pId;
      addList = new ChangeList();
      removeList = new ChangeList();
      // TODO insert define for has of Proto P0
      base = PrototypeExpression.ZERO;
    }

    /**
     * Set the Base link
     * 
     * @param pLink
     * @return Builder
     */
    public Builder setBase(Link pLink) {
      Preconditions.checkNotNull(pLink);
      base = pLink;
      return this;
    }

    /**
     * Set the Base link
     * 
     * @param pLink
     * @return Builder
     */
    public Builder setBase(PublishedPrototypeExpression pPe) {
      Preconditions.checkNotNull(pPe);
      base = Link.fromHash(pPe.getHash());
      return this;
    }

    /**
     * Add a published prototype expression to the add set.
     * 
     * @param pName
     * @param pProto
     * @return Builder
     */
    public Builder add(ID pName, PublishedPrototypeExpression pProto) {
      // TODO What about IPFS links?
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pProto);
      
      //TODO convert name IRI such that 
      
      return this.add(pName, pProto.getIpfsLink());
    }

    /**
     * Add a published prototype expression to the add set by giving its name
     * 
     * @param pName
     * @param pLink
     * @return Builder
     */
    public Builder add(ID pName, Link pLink) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pLink);
      addList.add(pName, pLink);
      return this;
    }

    /**
     * Add a published prototype expression to the remove set.
     * 
     * @param pName
     * @param pProto
     * @return Builder
     */
    public Builder remove(ID pName, PublishedPrototypeExpression pProto) {
      // TODO What about IPFS links?
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pProto);
      return this.remove(pName, pProto.getIpfsLink());
    }

    /**
     * Add a published prototype expression to the remove set by giving its name
     * 
     * @param pName
     * @param pLink
     * @return Builder
     */
    public Builder remove(ID pName, Link pLink) {
      Preconditions.checkNotNull(pName);
      Preconditions.checkNotNull(pLink);
      removeList.add(pName, pLink);
      return this;
    }

    /**
     * Build the prototype expression.
     * 
     * @return Prototype Expression
     */
    public PrototypeExpression build() {
      return new PrototypeExpression(id, base, addList, removeList);
    }
  }

}
