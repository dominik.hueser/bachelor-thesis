package domhues.prototypes.knowledgebase;

import java.io.IOException;
import java.util.ArrayList;

import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.Link;
import domhues.prototypes.peer.PeerNode;

/**
 * Prototype knowledgebases contain a set of Prototype expressions saved as Links.
 * There to create a new Prototype KB.
 * This can be published or fixed.
 * 
 * @author dominikhuser
 *
 */
public class PrototypeKB {

  private ArrayList<Link> prototypeExpressions;
  private ID name;

  /**
   * Construct a new with name pName
   * 
   * @param pName Name
   */
  PrototypeKB(ID pName, ArrayList<Link> pList) {
    prototypeExpressions = pList;
    name = pName;
  }

  /**
   * Returns the set of Prototype expression links which occurs in the knowledge base
   * 
   * @return
   */
  public ArrayList<Link> getPrototypeExpressions() {
    return prototypeExpressions;
  }

  /**
   * Get name.
   * @return
   */
  public ID getName() {
    return name;
  }
  
  /**
   * Alaternative way of putting a prototype KB.
   * Does not update the DIR!
   * @param peer
   * @throws IOException
   */
  public void putOn(PeerNode peer) throws IOException{
    peer.putKnowledgeBase(this, false);
  }
  
  
  /**
   * Try to create a fixed version of your kb.
   * You should publish the dir (publishDir) before calling that method. 
   * Otherwise used mutable links wont be found. 
   * @param peer
   * @return
   * @throws IOException
   */
  public FixedPrototypeKB fix(PeerNode peer) throws IOException{
    //TODO throw error if it was not possible
    return new FixedPrototypeKB(name, prototypeExpressions, peer);
  }
  
  public static class Builder{
    private ArrayList<Link> prototypeExpressions;
    private ID name;
    
    /**
     * Create new Builder of a KB with name pName
     * @param pName is ID
     */
    public Builder(ID pName){
      name = pName;
      prototypeExpressions = new ArrayList<Link>();
    }
    
    /**
     * Add PE to KB.
     * @param pProto
     * @return
     */
    public Builder add(PublishedPrototypeExpression pProto) {
      add(pProto.getIpfsLink());
      return this;
    }

    /**
     * Add Link to KB.
     * @param pLink
     * @return
     */
    public Builder add(Link pLink) {
      // TODO Throw exception if it is not a published prototype => Check link
      prototypeExpressions.add(pLink);
      return this;
    }
    
    /**
     * Remove PE from KB
     * @param pProto Prototype Expression
     * @return
     */
    public Builder remove(PublishedPrototypeExpression pProto) {
      remove(pProto.getIpfsLink());
      return this;
    }
    
    /**
     * Remove Link from KB.
     * @param pLink
     * @return
     */
    public Builder remove(Link pLink) {
      prototypeExpressions.remove(pLink);
      return this;
    }
    
    /**
     * Creates a new KB with pKb as initial prototype expression set and its name as new name.
     * @param pKb
     * @return
     */
    public static Builder modify(PrototypeKB pKb){
      Builder builder = new Builder(pKb.getName());
      for(Link l : pKb.getPrototypeExpressions()){
        builder.add(l);
      }
      return builder;
    }  
    
    /**
     * Build
     * @return
     */
    public PrototypeKB build(){
      return new PrototypeKB(name, prototypeExpressions);
    }
    
  }
}
