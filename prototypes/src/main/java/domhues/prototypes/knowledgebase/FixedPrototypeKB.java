package domhues.prototypes.knowledgebase;

import io.ipfs.multihash.Multihash;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.base.Preconditions;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;

import domhues.prototypes.expressions.ChangeList;
import domhues.prototypes.expressions.Prototype;
import domhues.prototypes.expressions.Prototype.PrototypeProperty;
import domhues.prototypes.expressions.PublishedChangeExpression;
import domhues.prototypes.expressions.PublishedPrototypeExpression;
import domhues.prototypes.links.AllLink;
import domhues.prototypes.links.ID;
import domhues.prototypes.links.Link;
import domhues.prototypes.peer.Parser;
import domhues.prototypes.peer.PeerNode;


/**
 * Fixed PrototypeKB where all mutable links are immutable now.
 * With a fixed KB you can be sure that a change of a mutable link during computation of a fixpoint or checking consistency has no influences. 
 * @author dominikhuser
 *
 */
public class FixedPrototypeKB extends PrototypeKB{

  // Contains a mapping from the Multihash to the published prototype expression object which is belonging
  private HashMap<Multihash, PublishedPrototypeExpression> prototypeExpressionMap;
  // Contains a mapping from the multihash to the fixpoint representation of a published prototype expression.  
  private HashMap<Multihash, Prototype> fixpointMap;
  // Peer Node which is needed for downloading the set and computing Fixpoint and Consistency.
  PeerNode peer;
  
  /**
   * Constructor, called by Builder class
   * @param pName, KB ID
   * @param prototypeExpressions, List of Prototype Expressions
   * @param pPeer, PeerNode
   * @throws IOException
   */
  FixedPrototypeKB(ID pName, ArrayList<Link> prototypeExpressions, PeerNode pPeer) throws IOException {
    super(pName,prototypeExpressions);
    peer = pPeer;
    prototypeExpressionMap = new HashMap<Multihash, PublishedPrototypeExpression>();
    fixpointMap = new HashMap<Multihash, Prototype>();
    downloadPrototypeExpressions();
  }


  /**
   * Check whether the knowledge base is consistent according to Knowledge Representation on the Web revisited: the Case for Prototypes
   * Throws Error when not consistent
   * @throws IOException
   */
  public void consistencyCheck() throws IOException {

    // check if proto:P_0 is not inside.
    if (prototypeExpressionMap.containsKey(PeerNode.PROTO0)) {
      throw new Error("Prototype Expression proto:P_0 is in the KB");
    }

    // check if every add expression and every base is defined in the KB
    // look up whether target hashes of base and add are in the hashmap as well.
    //
    for (PublishedPrototypeExpression current : prototypeExpressionMap.values()) {
      // check base
      if (prototypeExpressionMap.containsKey(peer.ipfsApi.resolveLink(current.getBase()))) {
        ChangeList changeList = current.getAddList();
        PublishedChangeExpression prop;
        // check add
        for (int i = 0; i < changeList.getSize(); i++) {
          prop = (PublishedChangeExpression) changeList.getPropertyAt(i);
          if (!prototypeExpressionMap.containsKey(peer.ipfsApi.resolveLink(prop.getLink()))) {
            throw new Error("property out of the ADD change set is not in the KB.");
          }
        }
      } else if (!peer.ipfsApi.resolveLink(current.getBase()).equals(PeerNode.PROTO0)) {
        throw new Error("Base is not in KB.");
      }
    }

    // check if grounded ( = no cycles)
    ImmutableGraph<String> imGraph = computeGraph(prototypeExpressionMap, peer);
    // throws error if a cycle is detected.
    Parser.cycleDetection(imGraph);



  }

  /**
   * helper for consistency checking
   * @param pMap
   * @param peer
   * @return
   * @throws IOException
   */
  private static ImmutableGraph<String> computeGraph(HashMap<Multihash, PublishedPrototypeExpression> pMap, PeerNode peer) throws IOException {
    Preconditions.checkNotNull(pMap);
    MutableGraph<String> graph = GraphBuilder.directed().build();

    for (PublishedPrototypeExpression prot : pMap.values()) {
      graph.addNode(prot.getHash().toBase58());
      graph.putEdge(prot.getHash().toBase58(), peer.ipfsApi.resolveLink(prot.getBase()).toBase58());
    }
    return ImmutableGraph.copyOf(graph);
  }

  /**
   * Computes the fixpoint of a knowledge base according to Knowledge Representation on the Web revisited: the Case for Prototypes
   * 
   * @return boolean TODO in case of hash identification changes here.
   **/
  public HashMap<Multihash, Prototype> computeFixpoint() throws IOException {
    for (PublishedPrototypeExpression current : prototypeExpressionMap.values()) {
      if (!fixpointMap.containsKey(current.getHash())) {
        recursiveFixpointComputation(current);
      }
    }
    return fixpointMap;
  }

  /**
   * helper function for fixpoint computation
   * @param pProtoExp
   * @throws IOException
   */
  private void recursiveFixpointComputation(PublishedPrototypeExpression pProtoExp) throws IOException {
    Preconditions.checkNotNull(pProtoExp);
    Preconditions.checkNotNull(peer);
    // base
    Prototype proto = new Prototype(pProtoExp.getId(), pProtoExp.getHash());
    Prototype base;// = new Prototype("proto:P_0");
    Multihash baseLink = peer.ipfsApi.resolveLink(pProtoExp.getBase());
    if (fixpointMap.containsKey(baseLink) && !baseLink.toBase58().equals(PeerNode.PROTO0.toBase58())) {
      base = fixpointMap.get(baseLink);
    } else if (prototypeExpressionMap.containsKey(baseLink) && !baseLink.toBase58().equals(PeerNode.PROTO0.toBase58())) {
      recursiveFixpointComputation(prototypeExpressionMap.get(baseLink));
      base = fixpointMap.get(baseLink);
    } else if(baseLink.toBase58().equals(PeerNode.PROTO0.toBase58())){
      base = new Prototype(ID.of("proto:P_0"), PeerNode.PROTO0);
    }else{
      throw new Error(" Fixpoint computation: Base link not in KB.");
    }

    if (!(base == null)) {
      // Add from base
      for (PrototypeProperty prop : base.getProperties()) {
        proto.add(prop.getName(), prop.getValue());
      }

    }

    Multihash hashProp;
    PublishedChangeExpression puProp;
    Multihash targetId;

    // Remove
    for (int i = 0; i < pProtoExp.getRemoveList().getSize(); i++) {
      puProp = (PublishedChangeExpression) pProtoExp.getRemoveList().getPropertyAt(i);
      try {
        if (puProp.getLink() instanceof AllLink) {
          proto.remove(puProp.getName());
        } else {
          hashProp = peer.ipfsApi.resolveLink(puProp.getLink());
          targetId = prototypeExpressionMap.get(hashProp).getHash();
          proto.remove(puProp.getName(), targetId);
        }
      } catch (IOException e) {
        // no handling needed.
        // When a remove change expression is not found in the kb. Then it will be ignored (According to definition).
      }
    }

    // Add from current Prototype Expression
    for (int i = 0; i < pProtoExp.getAddList().getSize(); i++) {

      puProp = (PublishedChangeExpression) pProtoExp.getAddList().getPropertyAt(i);
      hashProp = peer.ipfsApi.resolveLink(puProp.getLink());
      if (prototypeExpressionMap.get(hashProp) == null) {
        System.out.println(hashProp.toBase58());
      }
      targetId = prototypeExpressionMap.get(hashProp).getHash();

      proto.add(puProp.getName(), targetId);
    }

    fixpointMap.put(pProtoExp.getHash(), proto);
  }
  
  /**
   * downloads a fixed version out of the list of links.
   * @throws IOException
   */
  private void downloadPrototypeExpressions() throws IOException {
    PublishedPrototypeExpression proto;
    for (Link link : this.getPrototypeExpressions()) {
      proto = peer.getPrototypeExpression(link, true);

      prototypeExpressionMap.put(proto.getHash(), proto);
    }
  }
   
}
