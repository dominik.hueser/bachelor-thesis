Read Me 

1) Install two IPFS peer instances via https://ipfs.io/docs/getting-started/ and https://stackoverflow.com/questions/40180171/how-to-run-several-ipfs-nodes-on-a-single-machine 
	Interconnect them if they don't do that by their own (https://ipfs.io/docs/commands/#ipfs-swarm-connect)

2) Clone this project.

3) Change Maven test classes:
	Go to ./prototypes/src/test/java/domhues/prototypes
	Change in each .java file the IPFS configuration (IP, Port, PeerID to your IPFS daemon)
	The peer's ID can be found in its config file.

4) Run Maven "clean install" to get needed libraries listed in the pom.xml and run the test classes

5) EXAMPLE USAGE:
	Compile ExampleUsage.java in the examples package to jar
	Hand over arguments: ip_peer1 port_peer1 peerID_peer1 ip_peer2 port_peer2 peerID_peer2

5) BENCHMARKS:
	Compile Benchmark.java in the examples package to jar
	Hand over arguments: ip_peer1 port_peer1 peerID_peer1 ip_peer2 port_peer2 peerID_peer2
	For offline benchmarks disconnect your device from the internet.


